<?php
//Tournament states:
//    0 - not started,
//    1 - started,
//    2 - paused,
//    3 - finished,
//    4 - closed

if (session_status() == PHP_SESSION_NONE)
{
    session_start();
}
if (!$_SESSION['logged'])
    header('Location: index.php');

require_once('configs/configs.php');
require_once('Smarty.php');

unset($_SESSION['id_tournament']);
unset($_SESSION['tournamentRegSuccess']);
$tournaments = getTournaments($_SESSION['userEmail']);
$smarty->assign(array(
    'tournaments' => $tournaments ? $tournaments : array(),
    'title' => 'My tournaments'
));
$smarty->display('templates/tournaments.tpl');

function getTournaments($email)
{
    $sql = '
        SELECT t.*, COUNT(p.id_team) as countTeams
        FROM tournament t
        LEFT JOIN user u
        ON u.id_user = t.id_creator
        LEFT JOIN participant p
        ON p.id_tournament = t.id_tournament
        WHERE u.email = "'.$email.'"
        GROUP BY id_tournament';
    return executeS($sql);
}