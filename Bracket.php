<?php

class Bracket
{
    public $nRounds = 0;
    public $teams = array();
    public $html = '';
    public $bracket = array();
    public $rounds = array();

    public function __construct($teams, $nPools = 1)
    {
        $this->teams = $teams;
        $this->generateBracket($this->teams);
        $this->ajaxBracket();
    }

    public function ajaxBracket()
    {
        $this->updateBracketResults();
        if (isset($this->bracket['winner']))
        {
            $this->winner = $this->bracket['winner'];
            unset($this->bracket['winner']);
        }
        $this->regenerateBracket();
        $this->generateRounds();
    }

    public function getBracket()
    {
        $rounds = $this->rounds;
        $html = '<div class="bracket-container">';
        foreach ($rounds as $round)
        {
            $html .= $round;
            $html .= '<div class="round-table-placeholder"></div>';
        }
        $html .= '</div>';
        return $html;
    }

    public function generateBracket()
    {
        $teams = $this->teams;
        $nTeams = count($teams);
        $num_rounds = ceil(log($nTeams, 2));

        $bracket = array();
        $rounds = array();
        $empty_arr = array();

        for ($i = 0; $i < $num_rounds; $i++)
        {
            $rounds[$i] = '';

            $matches = ($nTeams*pow(.5, $i))/2;

            $nPreviousRoundMatches = $i > 0 ? ceil($nTeams*pow(.5, $i-1)/2) : 0;


            if ($i > 0)
            {
                if (($nPreviousRoundMatches % 2) > 0)
                {
                    if (!array_search('BYE', $teams))
                    {
                        if ($i % 2 == 0)
                            array_push($teams, 'BYE');
                        else
                            array_unshift($teams, 'BYE');

                        $nTeams++;
                    }
                }
            }
            else
                if (((ceil($matches) - floor($matches)) != 0) && ((int)$matches != 0)) //not even number of pairs, add a bye
                    if (!array_search('BYE', $teams))
                    {
                        if ($i % 2 == 0)
                            array_push($teams, 'BYE');
                        else
                            array_unshift($teams, 'BYE');

                        $nTeams++;
                    }


            $empty_arr = $nPreviousRoundMatches > 0 ? array_fill(0, $nPreviousRoundMatches, NULL) : array();

            $rounds[$i] .= '<div class="round-table" rel="'.$i.'">';

            for ($j = 0; $j < $matches; $j++)
            {
                //team1 treatment
                if ($teams)
                {
                    if (count($teams) == 1 && reset($teams) == 'BYE')//one team and its bye
                    {
                        if ($i % 2 == 0)//even round
                        {
                            if ($j == (int)$matches)//last match
                            {
                                $team1 = array_slice($teams, 0, 1);
                                $bracket['round'.($i)]['match'.($j)]['team1'] = reset($team1);
                                $bracket['round'.($i)]['match'.($j)]['isT1'] = (int)!(reset($team1) === 'BYE');
                                $bracket['round'.($i)]['match'.($j)]['from'] = (reset($team1) === 'BYE') ? 'none' : key($team1);
                                $bracket['round'.($i)]['match'.($j)]['idT1'] = (reset($team1) === 'BYE') ? '0' : ($team1[0][0] === 'B' ? 0 : $team1[0][0]);
                                $teams = array();
                            }
                            else//not last match
                            {
                                reset($empty_arr);
                                $key = key($empty_arr);
                                $team1 = '';//'Winner of round '.($i-1).' match '.$key;
                                $bracket['round'.($i)]['match'.($j)]['team1'] = $team1;
                                $bracket['round'.($i)]['match'.($j)]['isT1'] = 0;
                                $bracket['round'.($i)]['match'.($j)]['from'] = $key;
                                $bracket['round'.($i)]['match'.($j)]['idT1'] = '0';
                                unset($empty_arr[$key]);
                            }
                        }
                        else//not even round
                        {
                            if ($j == 0)//first match
                            {
                                $team1 = array_slice($teams, 0, 1);
                                $bracket['round'.($i)]['match'.($j)]['team1'] = reset($team1);
                                $bracket['round'.($i)]['match'.($j)]['isT1'] = (int)!(reset($team1) === 'BYE');
                                $bracket['round'.($i)]['match'.($j)]['from'] = (reset($team1) === 'BYE') ? 'none' : key($team1);
                                $bracket['round'.($i)]['match'.($j)]['idT1'] = (reset($team1) === 'BYE') ? '0' : ($team1[0][0] === 'B' ? 0 : $team1[0][0]);
                                $teams = array();
                            }
                            else//not first match
                            {
                                reset($empty_arr);
                                $key = key($empty_arr);
                                $team1 = '';//'Winner of round '.($i-1).' match '.$key;
                                $bracket['round'.($i)]['match'.($j)]['team1'] = $team1;
                                $bracket['round'.($i)]['match'.($j)]['isT1'] = 0;
                                $bracket['round'.($i)]['match'.($j)]['from'] = $key;
                                $bracket['round'.($i)]['match'.($j)]['idT1'] = '0';
                                unset($empty_arr[$key]);
                            }
                        }
                    }
                    else//more than one team or one not bye team
                    {
                        $team1 = array_slice($teams, 0, 1);
                        $bracket['round'.($i)]['match'.($j)]['team1'] = reset($team1);
                        $bracket['round'.($i)]['match'.($j)]['isT1'] = (int)!(reset($team1) === 'BYE');
                        $bracket['round'.($i)]['match'.($j)]['from'] = (reset($team1) === 'BYE') ? 'none' : key($team1);
                        $bracket['round'.($i)]['match'.($j)]['idT1'] = (reset($team1) === 'BYE') ? '0' : ($team1[0][0] === 'B' ? 0 : $team1[0][0]);
                        unset($teams[key($team1)]);
                    }
                }
                else//no teams
                {
                    reset($empty_arr);
                    $key = key($empty_arr);
                    $team1 = '';//'Winner of round '.($i-1).' match '.$key;
                    $bracket['round'.($i)]['match'.($j)]['team1'] = $team1;
                    $bracket['round'.($i)]['match'.($j)]['isT1'] = 0;
                    $bracket['round'.($i)]['match'.($j)]['from'] = $key;
                    $bracket['round'.($i)]['match'.($j)]['idT1'] = '0';
                    unset($empty_arr[$key]);
                }
                //end of team1

                $teams = array_values($teams);

                //team2 treatment
                if ($teams)
                {
                    if (count($teams) == 1 && reset($teams) == 'BYE')
                    {
                        if ($i % 2 == 0)
                        {
                            if ($j == (int)$matches)
                            {
                                $team2 = array_slice($teams, 0, 1);
                                $bracket['round'.($i)]['match'.($j)]['team2'] = reset($team2);
                                $bracket['round'.($i)]['match'.($j)]['isT2'] = (int)!(reset($team2) === 'BYE');
                                $bracket['round'.($i)]['match'.($j)]['to'] = 'none';
                                $bracket['round'.($i)]['match'.($j)]['idT2'] = (int)!(reset($team2) === 'BYE') ? '0' : ($team2[0][0] === 'B' ? 0 : $team2[0][0]);
                                $teams = array();
                            }
                            else
                            {
                                reset($empty_arr);
                                $key = key($empty_arr);
                                $team2 = '';//'Winner of round '.($i-1).' match '.$key;
                                $bracket['round'.($i)]['match'.($j)]['team2'] = $team2;
                                $bracket['round'.($i)]['match'.($j)]['isT2'] = 1;
                                $bracket['round'.($i)]['match'.($j)]['to'] = $key;
                                $bracket['round'.($i)]['match'.($j)]['idT2'] = '0';
                                unset($empty_arr[$key]);
                            }
                        }
                        else
                        {
                            if ($j == 0)
                            {
                                $team2 = array_slice($teams, 0, 1);
                                $bracket['round'.($i)]['match'.($j)]['team2'] = reset($team2);
                                $bracket['round'.($i)]['match'.($j)]['isT2'] = (int)!(reset($team2) === 'BYE');
                                $bracket['round'.($i)]['match'.($j)]['to'] = (reset($team2) === 'BYE') ? 'none' : key($team2);
                                $bracket['round'.($i)]['match'.($j)]['idT2'] = (reset($team2) === 'BYE') ? '0' : ($team2[0][0] === 'B' ? 0 : $team2[0][0]);
                                $teams = array();
                            }
                            else
                            {
                                reset($empty_arr);
                                $key = key($empty_arr);
                                $team2 = '';//'Winner of round '.($i-1).' match '.$key;
                                $bracket['round'.($i)]['match'.($j)]['team2'] = $team2;
                                $bracket['round'.($i)]['match'.($j)]['isT2'] = 1;
                                $bracket['round'.($i)]['match'.($j)]['to'] = $key;
                                $bracket['round'.($i)]['match'.($j)]['idT2'] = '0';
                                unset($empty_arr[$key]);
                            }
                        }
                    }
                    else
                    {
                        $team2 = array_slice($teams, 0, 1);
                        $bracket['round'.($i)]['match'.($j)]['team2'] = reset($team2);
                        $bracket['round'.($i)]['match'.($j)]['isT2'] = (int)!(reset($team2) === 'BYE');
                        $bracket['round'.($i)]['match'.($j)]['to'] = (reset($team2) === 'BYE') ? 'none' : key($team2);
                        $bracket['round'.($i)]['match'.($j)]['idT2'] = (reset($team2) === 'BYE') ? '0' : ($team2[0][0] === 'B' ? 0 : $team2[0][0]);
                        unset($teams[key($team2)]);
                    }
                }
                else
                {
                    reset($empty_arr);
                    $key = key($empty_arr);
                    $team2 = '';//'Winner of round '.($i-1).' match '.$key;
                    $bracket['round'.($i)]['match'.($j)]['team2'] = $team2;
                    $bracket['round'.($i)]['match'.($j)]['isT2'] = 1;
                    $bracket['round'.($i)]['match'.($j)]['to'] = $key;
                    $bracket['round'.($i)]['match'.($j)]['idT2'] = '0';
                    unset($empty_arr[$key]);
                }
                //end of team2

                $teams = array_values($teams);
                $results = array();
                if ($bracket['round'.($i)]['match'.($j)]['idT1'] && $bracket['round'.($i)]['match'.($j)]['idT2'])
                    $results = $this->getMatchResult($bracket['round'.($i)]['match'.($j)]['idT1'], $bracket['round'.($i)]['match'.($j)]['idT2']);
                $rounds[$i] .= '
                    <div class="bracket-table-row-container">
                        <table class="bracket-table-row">
                            <tr class="bracket" players="'.($bracket['round'.($i)]['match'.($j)]['isT1'] * $bracket['round'.($i)]['match'.($j)]['isT2']).'" rel="'.$j.'" from="'.$bracket['round'.($i)]['match'.($j)]['from'].'" to="'.$bracket['round'.($i)]['match'.($j)]['to'].'">
                                <td>


                                    <div class="'.($bracket['round'.($i)]['match'.($j)]['team1'] == 'BYE' ? 'team1 bye' : 'team1').'" id_team="'.$bracket['round'.($i)]['match'.($j)]['idT1'].'" title="'.(is_array($bracket['round'.($i)]['match'.($j)]['team1']) && array_key_exists(1, $bracket['round'.($i)]['match'.($j)]['team1']) ? $bracket['round'.($i)]['match'.($j)]['team1'][1] : $bracket['round'.($i)]['match'.($j)]['team1']).'">
                                        <span class="bracket-content">'.(is_array($bracket['round'.($i)]['match'.($j)]['team1']) && array_key_exists(1, $bracket['round'.($i)]['match'.($j)]['team1']) ? $bracket['round'.($i)]['match'.($j)]['team1'][1] : $bracket['round'.($i)]['match'.($j)]['team1']).'</span>
                                        <div class="result-container pull-right text-center" title="'.($bracket['round'.($i)]['match'.($j)]['team1'] == 'BYE' ? '0' : ($bracket['round'.($i)]['match'.($j)]['team2'] == 'BYE' ? '1' : (isset($results['score']) ? $results['score']['t1score'] : '--'))).'">'.($bracket['round'.($i)]['match'.($j)]['team1'] == 'BYE' ? '0' : ($bracket['round'.($i)]['match'.($j)]['team2'] == 'BYE' ? '1' : (isset($results['score']) ? $results['score']['t1score'] : '--'))).'</div>
                                    </div>
                ';
                $rounds[$i] .= '

                                    <div class="'.($bracket['round'.($i)]['match'.($j)]['team2'] == 'BYE' ? 'team2 bye' : 'team2').'" id_team="'.$bracket['round'.($i)]['match'.($j)]['idT2'].'" title="'.(is_array($bracket['round'.($i)]['match'.($j)]['team2']) && array_key_exists(1, $bracket['round'.($i)]['match'.($j)]['team2']) ? $bracket['round'.($i)]['match'.($j)]['team2'][1] : $bracket['round'.($i)]['match'.($j)]['team2']).'">
                                        <span class="bracket-content">'.(is_array($bracket['round'.($i)]['match'.($j)]['team2']) && array_key_exists(1, $bracket['round'.($i)]['match'.($j)]['team2']) ? $bracket['round'.($i)]['match'.($j)]['team2'][1] : $bracket['round'.($i)]['match'.($j)]['team2']).'</span>
                                        <div class="result-container pull-right text-center" title="'.($bracket['round'.($i)]['match'.($j)]['team2'] == 'BYE' ? '0' : ($bracket['round'.($i)]['match'.($j)]['team1'] == 'BYE' ? '1' : (isset($results['score']) ? $results['score']['t2score'] : '--'))).'">'.($bracket['round'.($i)]['match'.($j)]['team2'] == 'BYE' ? '0' : ($bracket['round'.($i)]['match'.($j)]['team1'] == 'BYE' ? '1' : (isset($results['score']) ? $results['score']['t2score'] : '--'))).'</div>
                                    </div>


                                </td>
                            </tr>
                        </table>
                    </div>';
            }
            $rounds[$i] .= '</div>';
        }
        $this->rounds = $rounds;
        $this->bracket = $bracket;
    }

    public function generateRounds()
    {
        $bracket = $this->bracket;
        $rounds = array();
        for ($i=0; $i<count($bracket); $i++)
        {
            $rounds[$i] = '<div class="round-table" rel="'.$i.'">';
            for ($j=0; $j<count($bracket['round'.$i]); $j++)
            {
                $rounds[$i] .= '
                    <div class="bracket-table-row-container">
                        <table class="bracket-table-row">
                            <tr class="bracket" players="'.($bracket['round'.($i)]['match'.($j)]['isT1'] * $bracket['round'.($i)]['match'.($j)]['isT2']).'" rel="'.$j.'" from="'.$bracket['round'.($i)]['match'.($j)]['from'].'" to="'.$bracket['round'.($i)]['match'.($j)]['to'].'">
                                <td>


                                    <div class="'.($bracket['round'.($i)]['match'.($j)]['team1'] == 'BYE' ? 'team1 bye' : 'team1').'" id_team="'.$bracket['round'.($i)]['match'.($j)]['idT1'].'" title="'.(is_array($bracket['round'.($i)]['match'.($j)]['team1']) && array_key_exists(1, $bracket['round'.($i)]['match'.($j)]['team1']) ? $bracket['round'.($i)]['match'.($j)]['team1'][1] : $bracket['round'.($i)]['match'.($j)]['team1']).'">
                                        <span class="bracket-content">'.(is_array($bracket['round'.($i)]['match'.($j)]['team1']) && array_key_exists(1, $bracket['round'.($i)]['match'.($j)]['team1']) ? $bracket['round'.($i)]['match'.($j)]['team1'][1] : $bracket['round'.($i)]['match'.($j)]['team1']).'</span>
                                        <div class="result-container pull-right text-center" title="'.($bracket['round'.($i)]['match'.($j)]['team1'] == 'BYE' ? '0' : ($bracket['round'.($i)]['match'.($j)]['team2'] == 'BYE' ? '1' : (isset($bracket['round'.($i)]['match'.($j)]['t1score']) ? $bracket['round'.($i)]['match'.($j)]['t1score'] : '--'))).'">'.($bracket['round'.($i)]['match'.($j)]['team1'] == 'BYE' ? '0' : ($bracket['round'.($i)]['match'.($j)]['team2'] == 'BYE' ? '1' : (isset($bracket['round'.($i)]['match'.($j)]['t1score']) ? $bracket['round'.($i)]['match'.($j)]['t1score'] : '--'))).'</div>
                                    </div>



                                    <div class="'.($bracket['round'.($i)]['match'.($j)]['team2'] == 'BYE' ? 'team2 bye' : 'team2').'" id_team="'.$bracket['round'.($i)]['match'.($j)]['idT2'].'" title="'.(is_array($bracket['round'.($i)]['match'.($j)]['team2']) && array_key_exists(1, $bracket['round'.($i)]['match'.($j)]['team2']) ? $bracket['round'.($i)]['match'.($j)]['team2'][1] : $bracket['round'.($i)]['match'.($j)]['team2']).'">
                                        <span class="bracket-content">'.(is_array($bracket['round'.($i)]['match'.($j)]['team2']) && array_key_exists(1, $bracket['round'.($i)]['match'.($j)]['team2']) ? $bracket['round'.($i)]['match'.($j)]['team2'][1] : $bracket['round'.($i)]['match'.($j)]['team2']).'</span>
                                        <div class="result-container pull-right text-center" title="'.($bracket['round'.($i)]['match'.($j)]['team2'] == 'BYE' ? '0' : ($bracket['round'.($i)]['match'.($j)]['team1'] == 'BYE' ? '1' :  (isset($bracket['round'.($i)]['match'.($j)]['t2score']) ? $bracket['round'.($i)]['match'.($j)]['t2score'] : '--'))).'">'.($bracket['round'.($i)]['match'.($j)]['team2'] == 'BYE' ? '0' : ($bracket['round'.($i)]['match'.($j)]['team1'] == 'BYE' ? '1' :  (isset($bracket['round'.($i)]['match'.($j)]['t2score']) ? $bracket['round'.($i)]['match'.($j)]['t2score'] : '--'))).'</div>
                                    </div>


                                </td>
                            </tr>
                        </table>
                    </div>';
            }
            $rounds[$i] .= '</div>';
        }
        $this->rounds = $rounds;
    }

    public function regenerateBracket()
    {
        $bracket = $this->bracket;
        for ($i=1; $i<count($bracket); $i++)
        {
            for ($j=0; $j<count($bracket['round'.$i]); $j++)
            {

                if ($bracket['round'.$i]['match'.$j]['t1advance'] == -1)//t1 to advance
                {
                    $t1from = $bracket['round'.$i]['match'.$j]['from'] !== 'none' ? $bracket['round'.$i]['match'.$j]['from'] : -1;//at which match of previous round to look

                    if ($t1from > -1 && $bracket['round'.($i-1)]['match'.$t1from]['t1advance'] != -1 && $bracket['round'.($i-1)]['match'.$t1from]['t2advance'] != -1)//if that match result is set
                    {
                            $bracket['round'.$i]['match'.$j]['team1'] = $bracket['round'.($i-1)]['match'.$t1from]['t1advance'] ? $bracket['round'.($i-1)]['match'.$t1from]['team1'] : $bracket['round'.($i-1)]['match'.$t1from]['team2'];
                            $bracket['round'.$i]['match'.$j]['idT1'] = $bracket['round'.($i-1)]['match'.$t1from]['t1advance'] ? $bracket['round'.($i-1)]['match'.$t1from]['idT1'] : $bracket['round'.($i-1)]['match'.$t1from]['idT2'];
                    }
                    else if ($t1from == -1)
                        $bracket['round'.$i]['match'.$j]['idT1'] = 0;
                }

                if ($bracket['round'.$i]['match'.$j]['t2advance'] == -1)//t2 to advance
                {
                    $t2to = $bracket['round'.$i]['match'.$j]['to'] !== 'none' ? $bracket['round'.$i]['match'.$j]['to'] : -1;//at which match of previous round to look

                    if ($t2to > -1 && $bracket['round'.($i-1)]['match'.$t2to]['t1advance'] != -1 && $bracket['round'.($i-1)]['match'.$t2to]['t2advance'] != -1)//if that match result is set
                    {
                        $bracket['round'.$i]['match'.$j]['team2'] = $bracket['round'.($i-1)]['match'.$t2to]['t1advance'] == 1 ? $bracket['round'.($i-1)]['match'.$t2to]['team1'] : $bracket['round'.($i-1)]['match'.$t2to]['team2'];
                        $bracket['round'.$i]['match'.$j]['idT2'] = $bracket['round'.($i-1)]['match'.$t2to]['t1advance'] == 1 ? $bracket['round'.($i-1)]['match'.$t2to]['idT1'] : $bracket['round'.($i-1)]['match'.$t2to]['idT2'];
                    }
                    else if ($t2to == -1)
                        $bracket['round'.$i]['match'.$j]['idT2'] = 0;
                }
            }
        }
        $this->bracket = $bracket;
    }

    public function updateBracketResults()
    {
        $bracket = $this->bracket;
        for ($i2=0; $i2<count($bracket); $i2++)
        {
            for ($j2=0; $j2<count($bracket['round'.$i2]); $j2++)
            {
                for ($i=0; $i<count($bracket); $i++)
                    for ($j=0; $j<count($bracket['round'.$i]); $j++)
                    {
                        ///////////////////////////////////////////////
                        //FILLING BRACKET ARRAY WITH REQUIRED DATA
                        if (($bracket['round'.$i]['match'.$j]['idT1'] == 0 && $bracket['round'.$i]['match'.$j]['idT2'] == 0) &&
                            (($bracket['round'.$i]['match'.$j]['isT1'] == 0 && $bracket['round'.$i]['match'.$j]['isT2'] == 0) ||
                                ($bracket['round'.$i]['match'.$j]['isT1'] == 1 && $bracket['round'.$i]['match'.$j]['isT2'] == 0) ||
                                ($bracket['round'.$i]['match'.$j]['isT1'] == 0 && $bracket['round'.$i]['match'.$j]['isT2'] == 1)))
                        {//ADV vs ADV || ADV vs BYE || BYE vs ADV
                            $bracket['round'.$i]['match'.$j]['t1advance'] = -1;
                            $bracket['round'.$i]['match'.$j]['t2advance'] = -1;
                            $bracket['round'.$i]['match'.$j]['id_match'] = -1;
                            continue;
                        }

                        if (($bracket['round'.$i]['match'.$j]['idT1'] > 0 && $bracket['round'.$i]['match'.$j]['idT2'] == 0) && ($bracket['round'.$i]['match'.$j]['isT1'] == 1 && $bracket['round'.$i]['match'.$j]['isT2'] == 0)
                            && $bracket['round'.$i]['match'.$j]['team2'] == 'BYE')//T1 vs BYE
                        {
                            $bracket['round'.$i]['match'.$j]['t1advance'] = 1;
                            $bracket['round'.$i]['match'.$j]['t2advance'] = 0;
                            $bracket['round'.$i]['match'.$j]['id_match'] = 0;
                        }

                        if (($bracket['round'.$i]['match'.$j]['idT2'] > 0 && $bracket['round'.$i]['match'.$j]['idT1'] == 0) && ($bracket['round'.$i]['match'.$j]['isT2'] == 1 && $bracket['round'.$i]['match'.$j]['isT1'] == 0)
                            && $bracket['round'.$i]['match'.$j]['team1'] == 'BYE')//BYE vs T2
                        {
                            $bracket['round'.$i]['match'.$j]['t2advance'] = 1;
                            $bracket['round'.$i]['match'.$j]['t1advance'] = 0;
                            $bracket['round'.$i]['match'.$j]['id_match'] = 0;
                        }

                        if ($bracket['round'.$i]['match'.$j]['idT1'] > 0 && $bracket['round'.$i]['match'.$j]['idT2'] > 0)//T1 vs T2
                        {
                            $match_result = $this->getMatchResult($bracket['round'.$i]['match'.$j]['idT1'], $bracket['round'.$i]['match'.$j]['idT2']);

                            $bracket['round'.$i]['match'.$j]['t1advance'] = isset($match_result['t1won']) ? $match_result['t1won'] : -1;
                            $bracket['round'.$i]['match'.$j]['t2advance'] = isset($match_result['t2won']) ? $match_result['t2won'] : -1;
                            $bracket['round'.$i]['match'.$j]['id_match'] = isset($match_result['id_match']) ? $match_result['id_match'] : -1;

                            $bracket['round'.$i]['match'.$j]['t1score'] = $match_result['score']['t1score'];
                            $bracket['round'.$i]['match'.$j]['t2score'] = $match_result['score']['t2score'];
                        }
                        //END OF FILLING
                        //////////////////////////////////////////////////////
                    }
            }

            $i1 = $i2+1;
            if ($i1 == count($bracket))
                continue;
            for ($j1=0; $j1<count($bracket['round'.$i1]); $j1++)
            {
                if ($bracket['round'.$i1]['match'.$j1]['t1advance'] == -1)//team1 advances
                {
                    $from = $bracket['round'.$i1]['match'.$j1]['from'] !== 'none' ? $bracket['round'.$i1]['match'.$j1]['from'] : -1;

                    if ($from != -1)
                    {
                        if ($bracket['round'.($i1-1)]['match'.$from]['t1advance'] == 1)
                        {
                            $bracket['round'.$i1]['match'.$j1]['team1'] = $bracket['round'.($i1-1)]['match'.$from]['team1'];
                            $bracket['round'.$i1]['match'.$j1]['idT1'] = $bracket['round'.($i1-1)]['match'.$from]['idT1'];
                            $bracket['round'.$i1]['match'.$j1]['isT1'] = 1;
                        }
                        else if ($bracket['round'.($i1-1)]['match'.$from]['t2advance'] == 1)
                        {
                            $bracket['round'.$i1]['match'.$j1]['team1'] = $bracket['round'.($i1-1)]['match'.$from]['team2'];
                            $bracket['round'.$i1]['match'.$j1]['idT1'] = $bracket['round'.($i1-1)]['match'.$from]['idT2'];
                            $bracket['round'.$i1]['match'.$j1]['isT1'] = 1;
                        }
                    }
                }
                if ($bracket['round'.$i1]['match'.$j1]['t2advance'] == -1)//team2 advances
                {
                    $to = $bracket['round'.$i1]['match'.$j1]['to'] !== 'none' ? $bracket['round'.$i1]['match'.$j1]['to'] : -1;
                    if ($to != -1)
                    {
                        if ($bracket['round'.($i1-1)]['match'.$to]['t1advance'] == 1)
                        {
                            $bracket['round'.$i1]['match'.$j1]['team2'] = $bracket['round'.($i1-1)]['match'.$to]['team1'];
                            $bracket['round'.$i1]['match'.$j1]['idT2'] = $bracket['round'.($i1-1)]['match'.$to]['idT1'];
                            $bracket['round'.$i1]['match'.$j1]['isT2'] = 1;
                        }
                        else if ($bracket['round'.($i1-1)]['match'.$to]['t2advance'] == 1)
                        {
                            $bracket['round'.$i1]['match'.$j1]['team2'] = $bracket['round'.($i1-1)]['match'.$to]['team2'];
                            $bracket['round'.$i1]['match'.$j1]['idT2'] = $bracket['round'.($i1-1)]['match'.$to]['idT2'];
                            $bracket['round'.$i1]['match'.$j1]['isT2'] = 1;
                        }
                    }
                }
            }
        }
        $last_round = $bracket['round'.(count($bracket)-1)]['match0'];
        $winner = $last_round['t1advance'] == 1 ? $last_round['team1'][1] : ($last_round['t2advance'] == 1 ? $last_round['team2'][1] : -1);
        if ($winner != -1)
            $bracket['winner'] = $winner;
        $this->bracket = $bracket;
    }

    public function getMatchResult($id_t1, $id_t2)
    {
        $id_tournament = isset($_GET['id_tournament']) ? $_GET['id_tournament'] :  (isset($_POST['id_tournament']) ? $_POST['id_tournament'] : 0);

        require_once('connect.php');
        $sql = '
            SELECT a.won t1won, b.won t2won, a.id_match
            FROM match_participant a
            LEFT JOIN participant p1
            ON p1.id_participant = a.id_participant
            INNER JOIN match_participant b
            ON a.id_match = b.id_match
                AND a.id_match_participant != b.id_match_participant
            LEFT JOIN participant p2
            ON p2.id_participant = b.id_participant
            WHERE p1.id_team = "'.(int)$id_t1.'"
                AND p2.id_team = "'.(int)$id_t2.'"
                AND p1.id_tournament = "'.(int)$id_tournament.'"
                AND p2.id_tournament = "'.(int)$id_tournament.'"
            ';

        $data = getValue($sql);
        $data['score'] = $this->getMatchScore($data['id_match']);

        return $data;
    }

    public function getMatchScore($id_match)
    {
        require_once('connect.php');
        $sql = '
            SELECT b.value t1score, a.value t2score
            FROM match_result a
            INNER JOIN match_result b
            ON a.id_match = b.id_match
                AND a.id_match_participant != b.id_match_participant
            WHERE a.id_match = "'.(int)$id_match.'"
                AND b.id_match = "'.(int)$id_match.'"
            ';

        return getValue($sql);
    }
}