
var team1;
var team2;
$(document).ready(function(){
    $('#datetimepicker').datetimepicker();

    if ($('#edit-info').length > 0)
    {
        var el = $('#edit-info');
        $(el).click(function(){
            var popup_el = $('' +
                '<div class="popup-info well" id="popup">' +
                    '<div class="input-group">'+
                        '<h5>Tournament name: </h5>'+
                        '<input type="text" class="form-control input-lg center-block" id="tournamentName" name="tournamentName"/>'+

                        '<h5 class="tournament-desc">Tournament description: </h5>'+
                        '<div class="control mce-parent">'+
                        '    <textarea class="form-control" id="tournamentDescription" placeholder="Describe this tournament" name="tournamentDescription"></textarea>'+
                        '</div>'+
                    '</div>' +
                    '<button type="button" class="btn btn-primary btn-lg center-block addparticipantbtn" name="submitUpdateInformation" id="submitUpdateInformation"><i class="fa fa-edit"></i> Update information</span></button>' +
                '</div>'
            );

            var FF = !(window.mozInnerScreenX == null);
            var left = FF ? "auto" : $(window).width() / 2 - 200;
            //var top = FF ? "auto" : $(window).height() / 2 - 200;
            var top = 150;

            $(popup_el).bPopup({
                    position: [left, top],
                    follow: [true, true],
                    onOpen: function(){clearOldPopups();},
                    onClose: function(){refreshAndClearOldPopups();}
                },
                function(){
                    getTournamentInformation();
                }
            );

            tinymce.remove();
            tinyMCE.remove();
            tinymce.init({
                    selector:'textarea',
                    menubar:'false',
                    width:'550'
            });


            $('#submitUpdateInformation').click(function(){
                updateTournamentInformation($('#tournamentName').val(), tinymce.get('tournamentDescription').getContent());
            });
        });
    }

    if ($('.bracket-container').length > 0)
    {
        refreshTable();
    }

    if ($('.bracket').length > 0)
    {
        refreshAttributes();
        addOnclick();
    }

    $('#submitCloseTournament').click(function(e){
        if (!confirm('Are you sure you want to close this tournament? This action cannot be undone'))
        {
            e.preventDefault;
            return false;
        }
    });

    $('#registerToTournament').click(function(){
        var popup_el = $('' +
            '<div class="popup well" id="popup">' +
                '<div class="input-group">'+
                    '<input type="text" class="form-control input-lg center-block" id="searchParticipant" name="searchParticipant" placeholder="Enter user\'s name"/>'+
                    '<span class="input-group-addon"><i class="fa fa-search-plus"></i></span>' +
                '</div>' +
                '<div id="scrollbarContainer" class="ajaxSearchResultsContainer list-group default-skin"></div>' +
                '<button type="button" class="btn btn-primary btn-lg center-block addparticipantbtn" name="submitAddParticipant" id="submitAddParticipant"><i class="fa fa-plus-circle"></i> Register selected users</span></button>' +
            '</div>'
        );
        var FF = !(window.mozInnerScreenX == null);
        var left = FF ? "auto" : $(window).width() / 2 - 200;
        //var top = FF ? "auto" : $(window).height() / 2 - 200;
        var top = 150;

        $(popup_el).bPopup({
            position: [left, top],
            follow: [true, true],
            onOpen: clearOldPopups(),
            onClose: clearOldPopups()
        });

        $('#submitAddParticipant').click(function(){
            addParticipants();
            location.reload();
        });
    });

    $('#registerModerator').click(function(){
        var popup_el = $('' +
            '<div class="popup well" id="popup">' +
            '<div class="input-group">'+
            '<input type="text" class="form-control input-lg center-block" id="searchParticipant" name="searchParticipant" placeholder="Enter user\'s name"/>'+
            '<span class="input-group-addon"><i class="fa fa-search-plus"></i></span>' +
            '</div>' +
            '<div id="scrollbarContainer" class="ajaxSearchResultsContainer list-group default-skin"></div>' +
            '<button type="button" class="btn btn-primary btn-lg center-block addparticipantbtn" name="submitAddModererator" id="submitAddModererator"><i class="fa fa-plus-circle"></i> Add moderators</span></button>' +
            '</div>'
        );
        var FF = !(window.mozInnerScreenX == null);
        var left = FF ? "auto" : $(window).width() / 2 - 200;
        //var top = FF ? "auto" : $(window).height() / 2 - 200;
        var top = 150;

        $(popup_el).bPopup({
            position: [left, top],
            follow: [true, true],
            onOpen: clearOldPopups(),
            onClose: clearOldPopups()
        });

        $('#submitAddModererator').click(function(){
            addModerators();
            location.reload();
        });
    });

    $('.page-body').on('keyup', '#searchParticipant', function(){
        $('.ajaxSearchResult:not(.active)').remove();
        processFindUsers($(this).val(), 2);
    });

});

function updateTournamentInformation(name, description)
{
    $.ajax({
        type:   "POST",
        url:    "process.php",
        async: false,
        data: {
            'ajax' : true,
            'updateTournamentInformation' : 1,
            'name' : name,
            'description' : description,
            'id_tournament' : encodeURIComponent(id_tournament)
        },
        success: function (resp)
        {
            location.reload();
        }
    });
}

function getTournamentInformation()
{
    var desc = '';
    $.ajax({
        type:   "POST",
        url:    "process.php",
        async:  false,
        data: {
            'ajax' : true,
            'getTournamentInformation' : 1,
            'id_tournament' : encodeURIComponent(id_tournament)
        },
        success: function (resp)
        {
            var response = JSON.parse(resp);
            $('#tournamentName').val(response[0].name);
            desc = response[0].description;
        }
    });
    tinymce.get('tournamentDescription').setContent(desc);
}

function addOnclick()
{
    $('.bracket').click(function(){
        if ($(this).attr('players') == 0 || !canEdit || $('#tournament-winner').length > 0)
            return;

        var t1 = $(this).find('.team1').find('.bracket-content').html();
        var t2 = $(this).find('.team2').find('.bracket-content').html();
        var match = $(this).attr('rel');
        var round = $(this).parent().parent().parent().parent().attr('rel');
        var id_t1 = $(this).find('.team1').attr('id_team');
        var id_t2 = $(this).find('.team2').attr('id_team');
        team1 = {'id_team' : id_t1, 'round' : round, 'match' : match};
        team2 = {'id_team' : id_t2, 'round' : round, 'match' : match};

        var popup_el = $('' +
            '<div class="popup-result well" id="popup-result">' +
            '<h4 class="results-heading text-center">Match results <i class="fa fa-bullhorn"></i></h4>' +
            '<table class="result-table">' +
            '<tr>' +
            '<div class="result-container"> ' +
            '<td class="team-name-td">'+t1+'</td>' +
            '<td class="score-td"><input type="number" pattern="\d*" step="1" class="input-sm pull-right" id="t1result" name="t1result"/></td>'+
            '<td class="result-td"><button class="btn btn-primary btn-sm pull-right t1win">Winner</button></td>'+
            '</div>' +
            '</tr>' +
            '</table>' +
            '<table class="result-table">' +
            '<tr>' +
            '<div class="result-container"> ' +
            '<td class="team-name-td">'+t2+'</td>' +
            '<td class="score-td"><input type="number" pattern="\d*" step="1" class="input-sm pull-right" id="t2result" name="t2result"/></td>'+
            '<td class="result-td"><button class="btn btn-primary btn-sm pull-right t2win">Winner</button></td>'+
            '</div>' +
            '</tr>' +
            '</table>' +
            '<button type="button" class="btn btn-primary btn-sm center-block addparticipantbtn" id="saveResult"><i class="fa fa-floppy-o"></i> Save</span></button>' +
            '</div>'
        );

        $(popup_el).bPopup({
            position: ["auto", "auto"],
            follow: [true, true],
            onOpen: clearOldPopups(),
            onClose: clearOldPopups()
        });

        $('.t1win').click(function(){
            if ($(this).hasClass('btn-success'))
            {
                $('.t2win').removeClass('btn-danger');
                $('.t2win').addClass('btn-success');
                $(this).removeClass('btn-success');
                $(this).addClass('btn-danger');
                $(this).html('Loser');
                $('.t2win').html('Winner');
                return;
            }
            if ($(this).hasClass('btn-danger'))
            {
                $('.t2win').removeClass('btn-success');
                $('.t2win').addClass('btn-danger');
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-success');
                $(this).html('Winner');
                $('.t2win').html('Loser');
                return;
            }
            $('.t2win').removeClass('btn-primary');
            $('.t2win').addClass('btn-danger');
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-success');
            $(this).html('Winner');
            $('.t2win').html('Loser');

            return;
        });

        $('.t2win').click(function(){
            if ($(this).hasClass('btn-success'))
            {
                $('.t1win').removeClass('btn-danger');
                $('.t1win').addClass('btn-success');
                $(this).removeClass('btn-success');
                $(this).addClass('btn-danger');
                $(this).html('Loser');
                $('.t1win').html('Winner');
                return;
            }
            if ($(this).hasClass('btn-danger'))
            {
                $('.t1win').removeClass('btn-success');
                $('.t1win').addClass('btn-danger');
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-success');
                $(this).html('Winner');
                $('.t1win').html('Loser');
                return;
            }
            $('.t1win').removeClass('btn-primary');
            $('.t1win').addClass('btn-danger');
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-success');
            $(this).html('Winner');
            $('.t1win').html('Loser');

            return;
        });

        $('#saveResult').click(function(){
            var t1pts = $('#t1result').val();
            var t2pts = $('#t2result').val();
            var t1win = -1;

            if ($('.t1win').hasClass('btn-success') && $('.t2win').hasClass('btn-danger'))
                t1win = 1;
            else if ($('.t1win').hasClass('btn-danger') && $('.t2win').hasClass('btn-success'))
                t1win = 0;
            if (t1win == -1) {
                alert('Please select a winner!');
            } else {
                saveResult(team1.round, team1.match, team1.id_team, team2.id_team, t1pts, t2pts, t1win);
                $('#popup-result').bPopup().close();
                refreshTable();
            }
        });
    });
}

function refreshAttributes()
{
    $('.bracket').each(function(){
        if ($(this).find('.team1').children('.bracket-content').html() == '' ||
            $(this).find('.team1').children('.bracket-content').html() == 'BYE' ||
            $(this).find('.team2').children('.bracket-content').html() == '' ||
            $(this).find('.team2').children('.bracket-content').html() == 'BYE'
            )
            $(this).attr('players', '0');
        else
            $(this).attr('players', '1');
    });
}

function refreshAndClearOldPopups()
{
    clearOldPopups();
    location.reload();
}

function clearOldPopups()
{
    $('.b-modal .__b-popup1__').remove();
    $('#popup-result').remove();
    $('#popup').remove();
    delete $('#popup-result');
    delete $('#popup');
}

function saveResult(round, match, id_t1, id_t2, t1_pts, t2_pts, t1win)
{
    $.ajax({
        type:   "POST",
        url:    "process.php",
        async: false,
        data: {
            'ajax' : true,
            'id_t1' : id_t1,
            'id_t2' : id_t2,
            't1_pts' : t1_pts,
            't2_pts' : t2_pts,
            't1win' : t1win,
            'saveResult' : 1,
            'id_tournament' : encodeURIComponent(id_tournament)
        },
        success: function (resp)
        {
            var response = JSON.parse(resp);
            $('.bracket-container').remove();
            var data = (typeof(response.winner) != "undefined" && response.winner !== null) ? response.winner + response.bracket : response.bracket
            $('body').append(data);
            refreshAttributes();
            addOnclick();
            refreshResult(response.id_t1, response.id_t2, t1_pts, t2_pts, round, match);
        }
    });
}

function refreshResult(id_t1, id_t2, t1_pts, t2_pts, round, match)
{
    var round_container = $('.round-table[rel='+round+']');
    var match_container = $(round_container).find('.bracket[rel='+match+']');
    var t1_result_container = $(match_container).find('[id_team='+id_t1+']').find('.result-container');
    var t2_result_container = $(match_container).find('[id_team='+id_t2+']').find('.result-container');

    t1_result_container.html(t1_pts);
    t1_result_container.attr('title', t1_pts);

    t2_result_container.html(t2_pts);
    t2_result_container.attr('title', t2_pts);
}

function refreshTable()
{
    reorderBracket();
    setContainerHeight('.bracket-container');
    connectBracket();
}

function addParticipants()
{
    var participants =  [];
    $('.ajaxSearchResult.active').each(function(){
        participants.push($(this).attr('rel'));
    });

    console.log(participants);
    if (participants)
        $.ajax({
            type:   "POST",
            url:    "process.php",
            async: false,
            data: {
                'ajax' : true,
                'participants' : participants,
                'registerParticipants' : 1,
                'id_tournament' : encodeURIComponent(id_tournament)
            },
            success: function (resp)
            {
            }
        });
}

function addModerators()
{
    var participants =  [];
    $('.ajaxSearchResult.active').each(function(){
        participants.push($(this).attr('rel'));
    });
    if (participants)
        $.ajax({
            type:   "POST",
            url:    "process.php",
            async: false,
            data: {
                'ajax' : true,
                'participants' : participants,
                'registerModerators' : 1,
                'id_tournament' : encodeURIComponent(id_tournament)
            },
            success: function (resp)
            {
            }
        });
}

function processFindUsers(name_partial, func)
{
    var skip = getSelectedUsers();
    $.ajax({
        type:   "POST",
        url:    "process.php",
        data: {
            'ajax' : true,
            'name' : encodeURIComponent(name_partial),
            'function' : encodeURIComponent(func),
            'searchUser' : 1,
            'id_tournament' : encodeURIComponent(id_tournament),
            'skip' : skip
        },
        success: function (resp)
        {
            if (resp)
            {
                var resp_obj = $.parseJSON(resp);
                populateContainerWithData(resp_obj, $('#scrollbarContainer'));
            }
            $('#scrollbarContainer').customScrollbar();
        }
    });
}

function getSelectedUsers()
{
    var selected = [];
    $('.ajaxSearchResult.active').each(function(){
        selected.push($(this).attr('rel'));
    });
    return selected;
}

function populateContainerWithData(data, container)
{
    var row = '';

    for (var i=0; i < data.length; i++)
    {
        row = $('<div class="list-group-item ajaxSearchResult" rel="'+data[i].id_user+'">'+data[i].name+' ('+data[i].email+')</div>');
        if ($('.overview').length > 0)
        {
            $('.overview').append(row);
        }
        else
        {
            $(container).append(row);
        }
    }

    $('.ajaxSearchResult').not('.active').click(function(){
        $(this).toggleClass('active');
    });

}

function reorderBracket()
{
    var container_top = Number($('.bracket-container').css('top').slice(0, -2));
    var container_left = Number($('.bracket-container').css('left').slice(0, -2));
    if (!container_top)
        container_top = 0;
    if (!container_left)
        container_left = 0;
    var first_el = $('.round-table[rel="0"]').first();
    var first_el_off = $(first_el).offset();
    $('.round-table').each(function(){
        var round_elem = $(this);
        var prev_round_elem = $(round_elem).prev().prev();
        var tr = $(this).find('.bracket');
        $(tr).each(function(){
            var from = $(this).attr('from');
            var to = $(this).attr('to');
            if ((from == 0 && to == 0) || (from == 0 && to == 'none'))//first round divs
            {
                var left = container_left;
                var top = $(this).attr('rel') * $(this).height() + container_top;
                $(this).parent().parent().parent().css('top', top+$(this).attr('rel')*10).css('left', left);
                return;
            }
            if (from == 'none')//BYE div in first row
            {
                var align_to = $(round_elem).prev().prev().children('.bracket-table-row-container').find('[rel="'+to+'"]').parent().parent().parent();
                var off = $(align_to).offset();
                var top = off.top - first_el_off.top;
                var row = $(this);
                var left_offset_multiplier = $(round_elem).attr('rel');
                var left = left_offset_multiplier * $(round_elem).width() + container_left;
                $(this).parent().parent().parent().css('top', top).css('left', left);
                return;
            }
            if (to == 'none')//BYE div in last row
            {
                var align_to = $(round_elem).prev().prev().children('.bracket-table-row-container').find('[rel="'+from+'"]');
                var pos = $(align_to).offset();
                var top = pos.top;
                var row = $(this);
                var left_offset_multiplier = $(round_elem).attr('rel');
                var left = left_offset_multiplier * $(round_elem).width() + container_left;
                $(this).parent().parent().parent().css('top', top).css('left', left);
                return;
            }
            var from_el = $(round_elem).prev().prev().children('.bracket-table-row-container').find('[rel="'+from+'"]').parent().parent().parent();
            var to_el = $(round_elem).prev().prev().children('.bracket-table-row-container').find('[rel="'+to+'"]').parent().parent().parent();
            var left = from_el.width() * (Number($(prev_round_elem).attr('rel'))+1) + container_left;
            var from_el_top = Number($(from_el).css('top').slice(0, -2));
            var to_el_bottom = Number($(to_el).css('top').slice(0, -2))+$(to_el).height();
            var middle = (to_el_bottom - from_el_top)/2;
            var row_height = $(this).parent().parent().parent().height();
            var top = middle + from_el_top - row_height/2;
            $(this).parent().parent().parent().css('top', top).css('left', left);
        });


//        alert(nEarlierRoundMatches);
    });
}

function connectBracket()
{//@TODO connect rounds inbetween
    var placeholder_width = $('.round-table-placeholder').width();
    var w = placeholder_width / 4;
    var first_el_off = $('.bracket[to="0"][from="0"][rel="0"]').parent().parent().parent().offset();
    var first_container =  $('.bracket-table-row-container').first();
    var h = $(first_container).height() / 2;
    var width_connector = placeholder_width - w;

//    console.log(height_connector);


    var next_connected = false;

    $('.bracket-table-row-container').each(function(index){
        var container = $(this);
        var off_container = $(container).offset();
        var new_top =  h/2;
        var new_left = $(container).width();
        var height_connector = h / 2;
        var next_match = $(this).next('.bracket-table-row-container');
        var next_off = $(next_match).offset();

        //gets number of matches in this round
        var nMatches = 0;
        $(container).parent().children('.bracket-table-row-container').each(function() {
            var value = Number($(this).find('.bracket').attr('rel'));
            nMatches = (value > nMatches) ? value : nMatches;
        });
        nMatches++;

        var hasBye = Number(nMatches > 1) && nMatches % 2;//does round have a bye

        var byeFirst = Number($(container).parent().attr('rel')) % 2;//would bye be in first match

        var needsByeFix;//shows whether this match needs a fix for bye
        if (byeFirst)
            needsByeFix = hasBye && $(container).find('.bracket').attr('rel') == (nMatches-1);//if last match - fix it
        else
            needsByeFix = hasBye && $(container).find('.bracket').attr('rel') == 0;// if first match - fix it
        needsByeFix = Number(needsByeFix);

        var connector;
        var r_connector;
        var s_last_connector;

        //rounds straight connector variables
        var s_conn_height = 0;
        var s_conn_width = placeholder_width - w;
        var s_conn_left = new_left + w;
        var s_conn_last_top;

        //matches connector variables
        var conn_height;
        var conn_top;
        var conn_left;
        var conn_width;

        //rounds connector variables
        var r_conn_height;
        var r_conn_width;
        var r_conn_left;
        var r_conn_top;

        if (next_match.length)//if next match exists
        {
            if (needsByeFix)
            {
                s_conn_last_top = new_top + h/2;

                s_last_connector = $('<div class="bracket-straight-connector"></div>');
                $(s_last_connector).css({'top':s_conn_last_top, 'left': s_conn_left, 'width': s_conn_width, 'height': s_conn_height});
                $(container).append(s_last_connector);
            }
            else if (!next_connected)
            {
                conn_height = next_off.top - off_container.top + 2;
                conn_top = $(container).height()/2 - 1;
                conn_left = new_left + w;
                conn_width = width_connector / 2;

                r_conn_height = 0;
                r_conn_width = placeholder_width - w - conn_width;
                r_conn_left = conn_left + conn_width;
                r_conn_top = conn_top + conn_height/2;


                connector = $('<div class="bracket-inbetween-connector"></div>');
                $(connector).css({'top':conn_top, 'left': conn_left, 'width': conn_width, 'height':conn_height});
                $(container).append(connector);

                r_connector = $('<div class="bracket-round-connector"></div>');
                $(r_connector).css({'top':r_conn_top, 'left': r_conn_left, 'width': r_conn_width, 'height':r_conn_height});
                $(container).append(r_connector);

            }
        }
        else
        {
            if (needsByeFix)//bye last and this match needs a fix
            {
                s_conn_last_top = new_top + h/2;

                s_last_connector = $('<div class="bracket-straight-connector"></div>');
                $(s_last_connector).css({'top':s_conn_last_top, 'left': s_conn_left, 'width': s_conn_width, 'height': s_conn_height});
                $(container).append(s_last_connector);
            }
        }
        if (!needsByeFix)
            next_connected = !next_connected;

        var conn = $('<div class="bracket-connector"></div>');
        $(conn).css({'top':new_top, 'left':new_left, 'width': w, 'height':h});
        $(container).append(conn);
    });
}

function setContainerHeight(el)
{
    var first_round_bye = $('.bracket[to="none"][from="0"]');
    var first_round_last = $('.bracket[to="0"][from="0"]').last();
    var first_round_first = $('.bracket[to="0"][from="0"][rel="0"]');
    var off_first = $(first_round_first).offset();
    if (first_round_bye.length > 0)//if BYE in first round
    {
        var off = $(first_round_bye).offset();
        $(el).height(off.top + $(first_round_bye).height() - off_first.top);
    }
    else
    {
        var off = $(first_round_last).offset();
        $(el).height(off.top + $(first_round_last).height() - off_first.top);
    }
}