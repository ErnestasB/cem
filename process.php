<?php
//Tournament states:
//    0 - not started,
//    1 - started,
//    2 - paused,
//    3 - finished,
//    4 - closed

require_once('connect.php');

if (isset($_POST['ajax']))
{

    if (isset($_POST['getTournamentInformation']) && isset($_POST['id_tournament']))
    {
        $data = getTournamentData($_POST['id_tournament']);
        die(json_encode($data));
    }

    if (isset($_POST['updateTournamentInformation']) && isset($_POST['id_tournament']))
    {
        updateTournament($_POST['id_tournament'], $_POST['name'], $_POST['description']);
        die(1);
    }

    if (isset($_POST['searchUser']) && isset($_POST['name']) && isset($_POST['id_tournament']))
    {
        $skip = isset($_POST['skip']) ? $_POST['skip'] : array();
        $users = getNameSearchSuggestions($_POST['name'], $_POST['id_tournament'], $skip);
        $response = array();
        if (!$users)
            die(json_encode($response));

        for ($i=0;$i<count($users); $i++)
        {
            $response[$i]['id_user'] = $users[$i]['id_user'];
            $response[$i]['name'] = $users[$i]['name'];
            $response[$i]['email'] = $users[$i]['email'];
        }
        die(json_encode($response));
    }

    if (isset($_POST['registerParticipants']) && isset($_POST['participants']) && isset($_POST['id_tournament']))
    {
        $participants = $_POST['participants'];
        $resp = true;
        foreach ($participants as $participant)
            $resp &= processTournamentRegistration($_POST['id_tournament'], '', $participant, 2);

        $_SESSION['multiregSuccess'] = 1;
        die(json_encode($resp));
    }

    if (isset($_POST['registerModerators']) && isset($_POST['participants']) && isset($_POST['id_tournament']))
    {
        $participants = $_POST['participants'];
        $resp = true;
        foreach ($participants as $participant)
            $resp &= processTournamentRegistration($_POST['id_tournament'], '', $participant, 3);

        $_SESSION['modRegSuccess'] = 1;
        die(json_encode($resp));
    }

    if (isset($_POST['saveResult']) && isset($_POST['id_tournament']))
    {
        $id_tournament = $_POST['id_tournament'];
        $id_match = createMatch($id_tournament);

        $t1won = $_POST['t1win'];
        $t2won = $t1won == 1 ? 0 : ($t1won == 0 ? 1 : -1);
        $t1_part = getParticipantByUserId($id_tournament, $_POST['id_t1']);
        $t2_part = getParticipantByUserId($id_tournament, $_POST['id_t2']);

        if (!addMatchParticipant($id_match, $t1_part['id_participant'], $t1won) ||
            !addMatchParticipant($id_match, $t2_part['id_participant'], $t2won))
            die('0');

        $t1 = getMatchParticipantByParticipantAndMatch($t1_part['id_participant'], $id_match);
        $t2 = getMatchParticipantByParticipantAndMatch($t2_part['id_participant'], $id_match);
        if (!createMatchResult($t1, $_POST['t1_pts'], $id_match) || !createMatchResult($t2, $_POST['t2_pts'], $id_match))
            die('0');

        $teams_array = getTournamentParticipants($id_tournament);
        $tournament['teams'] = $teams_array;
        $teams_count = count($teams_array);
        if ($teams_array && $teams_count > 1)
        {
            $teams = array();
            foreach ($teams_array as $team)
                array_push($teams, array($team['id_team'], $team['name']));
            require_once('Bracket.php');
            $bracket = new Bracket($teams);
            $bracket->ajaxBracket();
            $result = array();
            if (isset($bracket->winner))
                $result['winner'] = '<span id="tournament-winner" class="label label-success tournament-winner">Tournament winner: '.$bracket->winner.'</span>';

            $result['bracket'] = $bracket->getBracket();
            $result['id_t1'] = $_POST['id_t1'];
            $result['id_t2'] = $_POST['id_t2'];

            die(json_encode($result));
        }
        die('1');
    }

    exit();
}//AJAX END

if (isset($_POST['submit-register']))
{
    if (processRegistration($_POST['email'], $_POST['password'], $_POST['name']))
    {
        $_SESSION['logged'] = true;
        $_SESSION['userEmail'] = $_POST['email'];
        $_SESSION['userName'] = $_POST['name'];
        $_SESSION['success'][] = 'Registration successful';
    }
    else
    {
        $_SESSION['logged'] = false;
        $_SESSION['errors'][] = 'Registration failed';
    }
}

if (isset($_POST['submit-login']))
{
    if (($data = processLogin($_POST['email'], $_POST['password'])) !== null)
    {
        $_SESSION['logged'] = true;
        $_SESSION['userEmail'] = $data['email'];
        $_SESSION['userName'] = $data['name'];
        $_SESSION['id_user'] = $data['id_user'];
        $_SESSION['success'][] = 'Login successful';
    }
    else
    {
        $_SESSION['logged'] = false;
        $_SESSION['errors'][] = 'Incorrect login data';
    }
}

if (isset($_POST['submit-new-tournament']))
{

    if (processCreateTournament(
            $_POST['tournament-name'],
            $_POST['tournament-sport'],
            $_POST['tournament-participants'],
            $_POST['tournament-desc'],
            $_SESSION['userEmail'],
            $_POST['tournament-start-date'])
        )
    {
        $_SESSION['success'][] = 'Tournament created successfully';
    }
    else
        $_SESSION['errors'][] = 'Tournament creation failed';
}

if (isset($_POST['id_tournament']))
{
    $id_tournament = (int)$_POST['id_tournament'];
    $_SESSION['id_tournament'] = $id_tournament;

    if (isset($_POST['submitTournamentRegistration']))
    {
        if (processTournamentRegistration($id_tournament, $_SESSION['userEmail']))
        {
            $_SESSION['tournamentRegSuccess'] = 1;
        }
        else $_SESSION['tournamentRegSuccess'] = 0;
    }

    if (isset($_POST['submitTournamentDeregistration']))
    {
        if (processTournamentDeregistration($id_tournament, $_SESSION['userEmail']))
        {
            $_SESSION['tournamentDeregSuccess'] = 1;
        }
        else $_SESSION['tournamentDeregSuccess'] = 0;
    }

    if (isset($_POST['removeUser']))
    {
        $id_user = $_POST['removeUser'];
        if (removeParticipant($id_tournament, $id_user))
        {
            $_SESSION['removeSuccess'] = 1;
            $_SESSION['success'][] = 'User successfully removed';
        }
        else
        {
            $_SESSION['removeSuccess'] = 0;
            $_SESSION['errors'][] = 'Unable to remove user';
        }
    }

    if (isset($_POST['removeMod']))
    {
        $id_user = $_POST['removeMod'];
        if (removeParticipant($id_tournament, $id_user))
        {
            $_SESSION['removeSuccess'] = 1;
            $_SESSION['success'][] = 'Moderator successfully removed';
        }
        else
        {
            $_SESSION['removeSuccess'] = 0;
            $_SESSION['errors'][] = 'Unable to remove moderator';
        }
    }

    if (isset($_POST['submitStartTournament']))
    {
        if (setTournamentState($id_tournament, 1))
        {
            $_SESSION['stateSuccess'] = 1;
            $_SESSION['success'][] = 'Tournament has started';
        }
        else
        {
            $_SESSION['stateSuccess'] = 0;
            $_SESSION['errors'][] = 'Unable to start tournament';
        }
    }

    if (isset($_POST['submitPauseTournament']))
    {
        if (setTournamentState($id_tournament, 2))
        {
            $_SESSION['stateSuccess'] = 1;
            $_SESSION['success'][] = 'Tournament was paused';
        }
        else
        {
            $_SESSION['stateSuccess'] = 0;
            $_SESSION['errors'][] = 'Unable to pause tournament';
        }
    }

    if (isset($_POST['submitResumeTournament']))
    {
        if (setTournamentState($id_tournament, 1))
        {
            $_SESSION['stateSuccess'] = 1;
            $_SESSION['success'][] = 'Tournament was resumed';
        }
        else
        {
            $_SESSION['stateSuccess'] = 0;
            $_SESSION['errors'][] = 'Unable to resume tournament';
        }
    }

    if (isset($_POST['submitCloseTournament']))
    {
        if (setTournamentState($id_tournament, 4))
        {
            $_SESSION['stateSuccess'] = 1;
            $_SESSION['success'][] = 'Tournament was closed';
        }
        else
        {
            $_SESSION['stateSuccess'] = 0;
            $_SESSION['errors'][] = 'Unable to close tournament';
        }
    }

    die(header('Location: tournament.php?id_tournament='.$id_tournament));
}

die(header('Location: index.php'));

function updateTournament($id_tournament, $name, $description)
{
    $sql = '
        UPDATE tournament
        SET name = "'.escape($name).'",
            description = "'.escape($description).'"
        WHERE id_tournament = "'.(int)$id_tournament.'"';

    return execute($sql);
}

function getTournamentData($id_tournament)
{
    $sql = '
        SELECT t.*, s.name sport
        FROM tournament t
        LEFT JOIN sport s
        ON s.id_sport = t.id_sport
        WHERE t.id_tournament = '.(int)$id_tournament;
    return executeS($sql);
}

function getTournamentParticipants($id_tournament, $id_function = 2)
{
    $sql = '
        SELECT u.id_user, u.name, p.id_team
        FROM participant p
        LEFT JOIN user u
        ON u.id_user = p.id_team
        WHERE p.id_tournament = "'.(int)$id_tournament.'"
            AND p.id_function IN ('.$id_function.')
            AND p.state = 1';
    return executeS($sql);
}

function getParticipantByUserId($id_tournament, $id_user)
{
    $sql = '
        SELECT p.*
        FROM participant p
        LEFT JOIN user u
        ON p.id_team = u.id_user
        WHERE p.id_tournament = "'.(int)$id_tournament.'"
            AND u.id_user = "'.(int)$id_user.'"';

    return getValue($sql);
}

function createMatch($id_tournament)
{
    $sql = '
        INSERT INTO `match`
          (id_tournament)
        VALUES ("'.(int)$id_tournament.'")';
//    var_dump($sql);
//    die();
    execute($sql);
    return getLastId();
}

function addMatchParticipant($id_match, $id_participant, $won = -1)
{
    $sql = '
        SELECT * FROM match_participant
        WHERE id_match = "'.(int)$id_match.'"
            AND id_participant = "'.(int)$id_participant.'"';
    if (getValue($sql))
    {
        $sql = '
            UPDATE match_participant
            SET won = "'.(int)$won.'"
            WHERE id_match = "'.(int)$id_match.'"
                AND id_participant = "'.(int)$id_participant.'"';
    }
    else
    {
        $sql = '
            INSERT INTO match_participant
              (id_match, id_participant, won)
            VALUES ("'.(int)$id_match.'", "'.(int)$id_participant.'", "'.(int)$won.'")';
    }

    return execute($sql);
}

function createMatchResult($id_match_participant, $value, $id_match, $id_result_type = 1)
{
    $sql = '
        INSERT INTO match_result
          (id_result_type, id_match_participant, `value`, `id_match`)
        VALUES
          ("'.(int)$id_result_type.'", "'.(int)$id_match_participant.'", "'.escape($value).'", "'.(int)$id_match.'")';

    return execute($sql);
}

function updateMatchWinner($id_match, $id_participant, $won = 1)
{
    $sql = '
        UPDATE match_participant
        SET
            `won` = "'.(int)$won.'"
        WHERE id_match = "'.(int)$id_match.'"
            AND id_participant = "'.(int)$id_participant.'"';

    return execute($sql);
}

function getMatchParticipantByParticipantAndMatch($id_participant, $id_match)
{
    $sql = '
            SELECT id_match_participant
            FROM match_participant
            WHERE id_participant = "'.(int)$id_participant.'"
                AND id_match = "'.(int)$id_match.'"';
    $return = getValue($sql);
    return $return['id_match_participant'];
}

function updateMatchResult($id_match_participant, $value)
{
    $sql = '
        UPDATE match_result
        SET
            `value` = "'.escape($value).'"
        WHERE id_match_participant = "'.(int)$id_match_participant.'"';

    return execute($sql);
}

function getNameSearchSuggestions($part, $id_tournament, $skip)
{
    if (!$part)
        return '';
    if (!$skip)
        $skip = '';
    else
    {
        $skip = implode(', ', $skip);
        $skip = ' AND u.id_user NOT IN ('.escape($skip).') ';
    }

    $sql = '
        SELECT u.id_user, u.name, u.email
        FROM user u
        LEFT JOIN tournament t
        ON t.id_tournament = "'.(int)$id_tournament.'"
        WHERE u.id_user NOT IN (SELECT id_team FROM participant WHERE id_tournament = "'.(int)$id_tournament.'")
            '.$skip.'
            AND u.id_user != t.id_creator
            AND u.name LIKE "%'.$part.'%"';

    return executeS($sql);
}

function processTournamentRegistration($id_tournament, $userEmail = '', $id_user = 0, $function = 2)
{//@TODO add teams support

    if (!$id_user)
    {
        $sql = '
            SELECT id_user, name
            FROM user
            WHERE email = "'.escape($userEmail).'"';
        $user = getValue($sql);
        $user = $user['id_user'];
    }
    else
        $user = $id_user;

    $sql = '
    INSERT INTO participant
        (id_tournament, id_user, id_team, id_function)
    VALUES
        ("'.(int)$id_tournament.'", "'.(int)$user.'", "'.(int)$user.'", "'.(int)$function.'")';

    return execute($sql);
}

function processTournamentDeregistration($id_tournament, $userEmail)
{//@TODO teams support
    $sql = '
        SELECT id_user, name
        FROM user
        WHERE email = "'.escape($userEmail).'"';
    $user = getValue($sql);
//    $sql = '
//        DELETE FROM team
//        WHERE
//          name = "'.$user['name'].'"';
//    if (execute($sql))
//    {
//        $id_team = getValue('SELECT LAST_INSERT_ID() as id_team FROM team');
//        $sql = '
//            INSERT INTO team_member
//                (id_team, id_user)
//            VALUES
//                ("'.escape($id_team['id_team']).'", "'.escape($user['id_user']).'")';
//        if (execute($sql))
//        {
            $sql = '
            DELETE FROM participant
            WHERE id_tournament = "'.escape($id_tournament).'"
                 AND id_team = "'.escape($user['id_user']).'"';

            return execute($sql);
//        }
//    }
//    return false;
}

function processRegistration($email, $password, $name)
{
    $salt = getSalt();
    $password = md5($salt.$password);
    $sql = '
        INSERT INTO user
            (`email`, `name`, `password`)
        VALUES
            ("'.escape($email).'", "'.escape($name).'", "'.escape($password).'")';
    return execute($sql);
}

function processLogin($email, $password)
{
    $salt = getSalt();
    $password = md5($salt.$password);
    $sql = '
        SELECT name, email
        FROM user
        WHERE email = "'.escape($email).'"
            AND password = "'.escape($password).'"';
    return getValue($sql);
}

function processCreateTournament($tname, $game, $maxParticipants, $desc, $creatorEmail, $start_date)
{


    $sql = '
        SELECT id_user
        FROM user
        WHERE email = "'.$creatorEmail.'"';
    $creatorData = getValue($sql);

    date_default_timezone_set('Europe/Vilnius');
    $creationDate = date('Y-m-d H:i');

    $sport = getValue($sql);

    $sql = '
        INSERT INTO tournament
          (`name`, id_sport, id_creator, current_participants, max_participants, description, state, start_date, creation_date)
        VALUES
          ("'.escape($tname).'", "'.(int)$game.'", "'.(int)$creatorData['id_user'].'", 0, "'.(int)$maxParticipants.'", "'.escape($desc).'", 0, "'.escape($start_date).'", "'.$creationDate.'")';

    return execute($sql);
}

function setTournamentState($id_tournament, $state)
{
    if (!in_array($state, array(0, 1, 2, 3, 4)))
        return false;
    $sql = '
        UPDATE tournament
        SET state = "'.(int)$state.'"
        WHERE id_tournament = "'.(int)$id_tournament.'"';
    return execute($sql);
}

function removeParticipant($id_tournament, $id_team)
{
    $sql = '
        DELETE FROM participant
        WHERE id_tournament = "'.(int)$id_tournament.'"
            AND id_team = "'.(int)$id_team.'"';
    return execute($sql);
}