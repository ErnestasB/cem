<body class="page-body">
<div class="placeholder50"></div>
<div class="col-lg-4"></div>
<div class="container col-lg-3">

	<form class="form-register" action="process.php" method="post" role="form" name="register-form" id="register-form">
		<div class="placeholder20"></div>
		<h2 class="form-register-heading">Registration</h2>
		<div class="placeholder20"></div>
		<input value="" class="form-control" placeholder="Email address" required autofocus="" type="email" name="email">
		<div class="placeholder5"></div>
		<input value="" class="form-control" placeholder="Name" autofocus="" type="text" name="name">
		<div class="placeholder5"></div>
		<input class="form-control" placeholder="Password" required type="password" name="password">
		<div class="col-lg-1"></div>
		<div class="col-lg-9">
			<button class="btn btn-primary btn-block register-button" type="submit" name="submit-register">Register</button>
		</div>
	</form>

</div> <!-- /container -->
</body>