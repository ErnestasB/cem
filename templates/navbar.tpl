<div class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="margin-bottom: 0 !important;">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">
				<img src="img/logo.png" height="45"/>
			</a>
		</div>
		<div class="col-sm-3 text-center" style="height:1px">
			{if isset($errors) && $errors}
				<div class="alert alert-danger alert-dismissable">
					{foreach from=$errors item=error}
						{$error}<br/>
					{/foreach}
				</div>
			{elseif isset($success) && $success}
				<div class="alert alert-success alert-dismissable">
					{foreach from=$success item=s}
						{$s}<br/>
					{/foreach}
				</div>
			{/if}
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav pull-right">
				<li>
					<a href="findTournament.php"><i class="fa fa-search"></i> Find a tournament</a>
				</li>
				{if !$logged}
					<li>
						<div class="navbar-form">
							<form method="post" action="process.php" class="form-inline" role="form" name="login-form" id="login-form">
								<div class="form-group">
									<label class="sr-only" for="login-email">Email address</label>
									<input type="email" class="form-control input-sm" id="login-email" placeholder="Enter email" name="email">
								</div>
								<div class="form-group">
									<label class="sr-only" for="login-pwd">Password</label>
									<input type="password" class="form-control input-sm" id="login-pwd" placeholder="Password" name="password">
								</div>
								<button type="submit" class="btn btn-primary btn-sm" name="submit-login">Sign in</button>
							</form>
						</div> <!-- /container -->
					</li>
					<li><a href="register.php">Register</a></li>
				{else}
					<li>
						<a href="createTournament.php"><i class="fa fa-trophy"></i> Create a tournament</a>
					</li>
					<li>
						<a href="" class="navbar-text-custom dropdown-toggle"  data-toggle="dropdown">
							{if isset($name)}
								<i class="glyphicon glyphicon-user"></i>
								{$name}
							{/if}
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu navbar-inverse">
							<li><a href="myTournaments.php" class="hover-color-custom navbar-text-custom">My tournaments</a></li>
							<li class="divider"></li>
							<li><a href="#" class="hover-color-custom navbar-text-custom"><i class="glyphicon glyphicon-cog"></i> Settings</a></li>
						</ul>
					</li>
					<li><a href="logout.php"><i class="glyphicon glyphicon-off"></i> Logout</a></li>
				{/if}
			</ul>
		</div><!--/.nav-collapse -->
	</div>
</div>