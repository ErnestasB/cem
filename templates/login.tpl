<div class="row">
	<div class="col-sm-5"></div>
	<div class="container col-sm-2">
		<form class="form-inline" role="form">
			<div class="form-group">
				<label class="sr-only" for="exampleInputEmail2">Email address</label>
				<input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email">
			</div>
			<div class="form-group">
				<label class="sr-only" for="exampleInputPassword2">Password</label>
				<input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password">
			</div>
			<div class="checkbox">
				<label>
					<input type="checkbox"> Remember me
				</label>
			</div>
			<button type="submit" class="btn btn-default">Sign in</button>
		</form>
	</div> <!-- /container -->
	<div class="col-sm-5"></div>
</div>