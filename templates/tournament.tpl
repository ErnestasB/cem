<script>
	var id_tournament = {$tournament.id_tournament};
	var canEdit = Number("{$logged && ($isModerator || $isCreator) && $tournament.state == 1}");
</script>

<body class="page-body">
<div class="row col-lg-12 placeholder60"></div>

	{if $tournament.isCreator}

		<div class="row">
			<div class="col-lg-3"></div>
			<form method="post" action="process.php" id="tournament-creator-actions">
				<input type="hidden" name="id_tournament" value="{$tournament.id_tournament}"/>
				<div class="btn-group before-button">
					{if $tournament.state != 4 && $tournament.state != 3}
						{if ($tournament.max_participants == 0 || $tournament.countTeams < $tournament.max_participants) && $tournament.state == 0}
							<button type="button" id="registerToTournament" class="btn btn-primary"><i class="fa fa-plus"></i> Register a participant</button>
						{/if}
						<button type="button" id="registerModerator" class="btn btn-default"><i class="fa fa-plus-circle"></i> Register tournament moderators</button>
						{if $tournament.state == 0}
							<button type="submit" name="submitStartTournament" class="btn btn-success"><i class="fa fa-play"></i> Start tournament</button>
						{elseif $tournament.state == 1}
							<button type="submit" name="submitPauseTournament" class="btn btn-warning"><i class="fa fa-pause"></i> Pause tournament</button>
						{elseif $tournament.state == 2}
							<button type="submit" name="submitResumeTournament" class="btn btn-success"><i class="fa fa-play"></i> Resume tournament</button>
						{/if}
						<button type="submit" id="submitCloseTournament" name="submitCloseTournament" class="btn btn-danger "><i class="fa fa-minus-circle"></i> Close tournament</button>
					{/if}
				</div>
			</form>
		</div>
		<div class="placeholder20"></div>
	{else}

		<div class="row">
			<div class="col-lg-4"></div>
			{if isset($logged) && $logged}
				<div class="btn-group before-button">
					<form method="post" action="process.php" id="tournament-registration">
						<input type="hidden" name="id_tournament" value="{$tournament.id_tournament}"/>
						{if $tournament.open_registration == 1 && !$tournament.isRegistered && $tournament.state == 0 && ($tournament.max_participants == 0 || $tournament.countTeams < $tournament.max_participants)}
							<button type="submit" id="submitTournamentRegistration" name="submitTournamentRegistration" class="btn btn-primary"><i class="fa fa-plus"></i> Register to this tournament</button>
						{elseif $tournament.isRegistered && !$isModerator && $tournament.state == 0}
							<button type="submit" class="btn btn-danger " id="submitTournamentDeregistration" name="submitTournamentDeregistration"><i class="fa fa-minus-circle"></i> Stop participating in this tournament</button>
						{/if}
					</form>
				</div>
			{/if}
		</div>

	{/if}

	<div class="col-lg-4 well">
		{*/*  Tournament states:      */*}
		{*/*      0 - not started,    */*}
		{*/*      1 - started,        */*}
		{*/*      2 - paused,         */*}
		{*/*      3 - finished,       */*}
		{*/*      4 - closed          */*}
		<h3 class="participants-heading">&nbsp<i class="fa fa-users"></i> Information {if $tournament.isCreator && !in_array($tournament.state, array(3,4))}<i id="edit-info" title="Edit information" class="fa fa-edit pull-right"></i>{/if}</h3>
		<h5 class="info-line">Tournament name: <span class="pull-right">{$tournament.name}</span></h5>
		<h5 class="info-line">Sport/Game: <span class="pull-right">{$tournament.sport}</span></h5>
		{if $tournament.description}<h5 ">Description:</h5> <div  class="tournament-description clearfix info-line">{$tournament.description}</div>{/if}
		{if $tournament.start_date && $tournament.start_date != '0000-00-00 00:00:00'}<h5 class="info-line">Start date: <span class="pull-right">{$tournament.start_date}</span></h5>{/if}
		<h5 class="info-line">Tournament state:
			{if $tournament.state == 0}
				<i class="fa fa-circle-o pull-right"><span class="state-text"> Not started</span></i>
			{elseif $tournament.state == 1}
				<i class="fa fa-play pull-right"><span class="state-text"> Started</span></i>
			{elseif $tournament.state == 2}
				<i class="fa fa-pause pull-right"><span class="state-text"> Paused</span></i>
			{elseif $tournament.state == 3}
				<i class="fa fa-flag-checkered pull-right"><span class="state-text"> Finished</span></i>
			{elseif $tournament.state == 4}
				<i class="fa fa-stop pull-right"><span class="state-text"> Closed</span></i>
			{/if}
		</h5>
		<h3 class="participants-heading">&nbsp<i class="fa fa-users"></i> Tournament participants</h3>
		<div class="progress">
			<div class="progress-bar {if $tournament.max_participants && $tournament.countTeams >= $tournament.max_participants}progress-bar-danger{else}progress-bar-info{/if}" role="progressbar"
				 aria-valuenow="{$tournament.countTeams}"
				 aria-valuemin="0"
				 aria-valuemax="{$tournament.max_participants}"
				 style="width: {if $tournament.max_participants > 0}{($tournament.countTeams*100)/$tournament.max_participants}{else}100{/if}%">

				<span class="progress-bar-text">{if $tournament.max_participants > 0}{$tournament.countTeams} / {$tournament.max_participants}{else}{$tournament.countTeams}{/if}</span>

			</div>
		</div>
		{if $tournament.countTeams == 0}
			<div class="col-lg-1"></div>
			<div class="text-center col-lg-9 alert alert-warning no-teams">This tournament has no participants yet</div>
		{else}
			<ul class="list-group">
				<form method="post" action="process.php">
					<input type="hidden" name="id_tournament" value="{$tournament.id_tournament}"/>
					{foreach from=$tournament.teams item=team}
						<li class="list-group-item" id="{$team.name}">
							{$team.name}
							{if $tournament.state == 0 && $id_user == $tournament.id_creator}
								<button type="submit" name="removeUser" value="{$team.id_team}" class="btn btn-danger btn-sm pull-right" title="Remove this user">
									<i class="fa fa-minus" ></i>
								</button>
							{/if}
						</li>
					{/foreach}
				</form>
			</ul>
		{/if}

		<h3 class="participants-heading">&nbsp<i class="fa fa-user-md"></i> Tournament moderators</h3>
		<div class="progress">
			<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width: 100%">
				<span class="progress-bar-text">{$tournament.countMods}</span>
			</div>
		</div>
		{if $tournament.countMods == 0}
			<div class="col-lg-1"></div>
			<div class="text-center col-lg-9 alert alert-warning no-teams">This tournament has no moderators yet</div>
		{else}
			<ul class="list-group">
				<form method="post" action="process.php">
					<input type="hidden" name="id_tournament" value="{$tournament.id_tournament}"/>
					{foreach from=$tournament.mods item=moderator}
						<li class="list-group-item" id="{$moderator.name}">
							{$moderator.name}
							{if $moderator.id_user != $tournament.id_creator && $id_user == $tournament.id_creator}
								<button type="submit" name="removeMod" value="{$moderator.id_user}" class="btn btn-danger btn-sm pull-right" title="Remove this moderator">
									<i class="fa fa-minus" ></i>
								</button>
							{/if}
						</li>
					{/foreach}
				</form>
			</ul>
		{/if}
	</div>

</body>