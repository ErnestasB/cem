<body class="page-body">
{include 'navbar.tpl'}
<div class="row col-lg-12" style="min-height: 60px;"></div>
<div class="placeholder80"></div>

<div class="col-lg-6">
	<div class="list-group">
		<div class="well well-lg">
			<h2 class="text-center">Today's tournaments</h2>
			<br/>
			<table class="todays-table">
				<tr class="todays-row">
					<td class="list-group-item-heading todays-heading">Tournament name</td>
					<td class="list-group-item-heading todays-heading">Sport or game</td>
					<td class="list-group-item-heading pull-right todays-heading todays-heading-last">Start date</td>
				</tr>
			</table>
			{foreach from=$todaysTournaments item=tournament}
				{if $tournament.id_tournament}
					{*/*  Tournament states:      */*}
					{*/*      0 - not started,    */*}
					{*/*      1 - started,        */*}
					{*/*      2 - paused,         */*}
					{*/*      3 - finished,       */*}
					{*/*      4 - closed          */*}
					<a href="tournament.php?id_tournament={$tournament.id_tournament}">
						<table class="todays-table">
							<tr class="todays-list-row list-group-item
								{if $tournament.state == 0}
									state0
								{elseif $tournament.state == 1}
									state1
								{elseif $tournament.state == 2}
									state2
								{elseif $tournament.state == 3}
									state3
								{elseif $tournament.state == 4}
									state4
								{/if}
							">
								<td class="todays-row-td-first">
									<i class="fa
									{if $tournament.state == 0}
										fa-circle-o" title="Not started"
									{elseif $tournament.state == 1}
										fa-play" title="Started"
									{elseif $tournament.state == 2}
										fa-pause" title="Paused"
									{elseif $tournament.state == 3}
										fa-flag-checkered" title="Finished"
									{elseif $tournament.state == 4}
										fa-stop" title="Closed"
									{/if}
									> </i> {$tournament.name}</td>
								<td class="todays-row-td-middle">{$tournament.sport}</td>
								<td class="todays-row-td-last">{if ($tournament.start_date) != '0000-00-00 00:00:00'}{$tournament.start_date}{else} -- {/if}</td>
							</tr>
						</table>
					</a>
				{/if}
			{/foreach}
		</div>
	</div>
</div>


<div class="col-lg-6">
	<div class="list-group">
		<div class="well well-lg">
			<h2 class="text-center">Overall statistics</h2>
			<br/>
			<h4 class="media-heading ">Sport or game <span class="media-heading pull-right">Number of tournaments</span></h4>
			{foreach from=$popularSports item=sport}
				<div class="list-group-item">
					{$sport.name}
					<div class="badge alert-info pull-right">{$sport.game_count}</div>
				</div>

			{/foreach}
		</div>
	</div>
</div>

</body>