<body class="page-body">
{include 'navbar.tpl'}
<div class="row col-lg-12" style="min-height: 60px;"></div>
<div class="col-lg-2"></div>
<div class="container col-lg-8">
	<h2 class="form-new-tournament-heading">{if isset($title)}{$title}{/if}</h2>
	<div class="placeholder20"></div>
	<div class="list-group">
		<div class="well">
			<h4 class="list-group-item-heading">Tournament name</h4>
			{if isset($tournaments) && $tournaments|@count > 0}
				{foreach from=$tournaments item=tournament}
					{if $tournament.id_tournament}
						{*/*  Tournament states:      */*}
						{*/*      0 - not started,    */*}
						{*/*      1 - started,        */*}
						{*/*      2 - paused,         */*}
						{*/*      3 - finished,       */*}
						{*/*      4 - closed          */*}
						<a href="tournament.php?id_tournament={$tournament.id_tournament}" class="list-group-item
						{if $tournament.state == 0}
							state0
						{elseif $tournament.state == 1}
							state1
						{elseif $tournament.state == 2}
							state2
						{elseif $tournament.state == 3}
							state3
						{elseif $tournament.state == 4}
							state4
						{/if}
						"> {$tournament.name} <span class="badge {if $tournament.countTeams >= $tournament.max_participants && $tournament.max_participants > 0} alert-danger{else} alert-success{/if}" title="Participants">{$tournament.countTeams}{if $tournament.max_participants > 0}/{$tournament.max_participants}{/if}</span><i class="pull-left list-state-icon fa
						{if $tournament.state == 0}
							fa-circle-o" title="Not started"
						{elseif $tournament.state == 1}
							fa-play" title="Started"
						{elseif $tournament.state == 2}
							fa-pause" title="Paused"
						{elseif $tournament.state == 3}
							fa-flag-checkered" title="Finished"
						{elseif $tournament.state == 4}
							fa-stop" title="Closed"
						{/if}
						></i></a>
					{/if}
				{/foreach}
			{/if}
		</div>
	</div>

</div>
</body>