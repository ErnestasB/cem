<body class="page-body">
{include 'navbar.tpl'}
<div class="row col-lg-12" style="min-height: 60px;"></div>
<div class="col-lg-2"></div>
<div class="container col-lg-8">
	<form method="post" action="process.php" class="form-inline" role="form" name="new-tournament-form" id="new-tournament-form">
		<h2 class="form-new-tournament-heading">Create a tournament</h2>
		<div class="placeholder50"></div>
		<div class="control-group">

			<label class="control-label label-left" for="tournament-name">Tournament name</label>
			<div class="control">
				<div class="input-group inline-flex">
					<input type="text" class="form-control tournament-input-sm" id="tournament-name" placeholder="Tournament name" name="tournament-name" required>
					<span class="input-group-addon form-control">
						<i class="fa fa-font"></i>
					</span>
				</div>
			</div>
			<div class="placeholder20"></div>
			<div class="clearfix"></div>

			<label class="control-label label-left" for="tournament-sport">Game</label>
			<div class="control">
				<div class="input-group inline-flex">
					<select class="form-control tournament-input-sm" id="tournament-sport" placeholder="The game or sport of tournament" name="tournament-sport">
						{foreach $sports as $sport}
							<option value="{$sport.id_sport}">{$sport.name}</option>
						{/foreach}
					</select>
					<span class="input-group-addon form-control">
						<i class="fa fa-gamepad"></i>
					</span>
				</div>
			</div>
			<div class="placeholder20"></div>
			<div class="clearfix"></div>

			<label class="control-label label-left" for="tournament-participants">Max. number of participants</label>
			<div class="control">
				<div class="input-group inline-flex">
					<input type="number" class="form-control tournament-input-xs" id="tournament-participants" placeholder="Leave blank for unlimited" name="tournament-participants">
					<span class="input-group-addon form-control">
						<i class="fa fa-users"></i>
					</span>
				</div>
			</div>
			<div class="placeholder20"></div>
			<div class="clearfix"></div>

			<label class="control-label label-left" for="tournament-desc">Description</label>
			<div class="control mce-parent">
				<textarea class="form-control" id="tournament-desc" placeholder="Describe this tournament" name="tournament-desc"></textarea>
			</div>
			<div class="placeholder20"></div>
			<div class="clearfix"></div>

			<label class="control-label label-left" for="datetimepicker">Tournament start date</label>
			<div class="control">
				<div class="input-group inline-flex">
				<input id="datetimepicker" class="form-control" placeholder="Click to pick a date" type="text" name="tournament-start-date">
				<span class="input-group-addon form-control">
					<i class="glyphicon glyphicon-calendar"></i>
				</span>
				</div>
			</div>
			<div class="clearfix"></div>

		</div>
		<div class="placeholder20"></div>

		<label class="label-left"></label>
		<button type="submit" class="btn btn-primary tournament-input-md" name="submit-new-tournament">Create</button>
	</form>
</div>
</body>