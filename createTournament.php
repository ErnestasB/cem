<?php
if (session_status() == PHP_SESSION_NONE)
{
    session_start();
}
if (!$_SESSION['logged'])
    header('Location: index.php');

require_once('configs/configs.php');
require_once('Smarty.php');

$smarty->assign('sports', getActiveSports());
$smarty->display('templates/new-tournament.tpl');

function getActiveSports()
{
    $sql = '
        SELECT *
        FROM sport
        WHERE active = 1';

    return executeS($sql);
}