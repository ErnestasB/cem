<?php
if (session_status() == PHP_SESSION_NONE)
{
    session_start();
}

require_once('configs/configs.php');
require_once('Smarty.php');

if (isset($_SESSION['id_tournament']))
    $id_tournament = (int)$_SESSION['id_tournament'];
if (isset($_GET['id_tournament']))
   $id_tournament = $_GET['id_tournament'];
if (!isset($id_tournament) || !$id_tournament)
    header('Location: index.php');

if (isset($_SESSION['multiregSuccess']))
{
    $smarty->assign('success', array('User(s) were successfully registered'));
    unset($_SESSION['multiregSuccess']);
}

if (isset($_SESSION['tournamentRegSuccess']))
{
    if ($_SESSION['tournamentRegSuccess'])
    {
        $smarty->assign(array(
            'success' => array('Successfully registered to this tournament')
        ));
    }
    else
    {
        $smarty->assign(array(
            'errors' => array('Registration to tournament failed')
        ));
    }
    unset($_SESSION['tournamentRegSuccess']);
}

if (isset($_SESSION['tournamentDeregSuccess']))
{
    if ($_SESSION['tournamentDeregSuccess'])
    {
        $smarty->assign(array(
            'success' => array('You were removed from this tournament')
        ));
    }
    else
    {
        $smarty->assign(array(
            'errors' => array('Failed to deregister from tournament')
        ));
    }
    unset($_SESSION['tournamentDeregSuccess']);
}

if (isset($_SESSION['stateSuccess']))
{
    $smarty->assign(array(
        'success' => isset($_SESSION['success']) ? $_SESSION['success'] : array(),
        'errors' => isset($_SESSION['errors']) ? $_SESSION['errors'] : array()
    ));
    unset($_SESSION['success']);
    unset($_SESSION['errors']);
}

if (isset($_SESSION['removeSuccess']))
{
    $smarty->assign(array(
        'success' => isset($_SESSION['success']) ? $_SESSION['success'] : array(),
        'errors' => isset($_SESSION['errors']) ? $_SESSION['errors'] : array()
    ));
    unset($_SESSION['success']);
    unset($_SESSION['errors']);
}


echo '<input type="hidden" name="id_tournament" value="'.$id_tournament.'"/>';

$userEmail = $_SESSION['userEmail'];
$sql = '
        SELECT id_user
        FROM user
        WHERE email = "'.escape($userEmail).'"';

$id_user = getValue($sql);
$tournament_array = getTournamentData($id_tournament);
$tournament = $tournament_array[0];

$tournament['isRegistered'] = isRegistered($id_user['id_user'], $id_tournament);
if ($tournament['id_creator'] == $id_user['id_user'])
    $tournament['isCreator'] = true;
else
    $tournament['isCreator'] = false;

$teams_array = getTournamentParticipants($id_tournament);
$tournament['teams'] = $teams_array;
$teams_count = count($teams_array);
$tournament['countTeams'] = $teams_count;
$mods = getTournamentParticipants($id_tournament, 3);
$creator = getTournamentCreator($id_tournament);
array_unshift($mods, $creator[0]);
$tournament['mods'] = $mods ? $mods : array();
$tournament['countMods'] = $mods ? count($mods) : 0;
if ($teams_array && $teams_count > 1)
{//generates and prints bracket
    $teams = array();
    foreach ($teams_array as $team)
        array_push($teams, array($team['id_team'], $team['name']));
    $bracket = new Bracket($teams);
    if (isset($bracket->winner))
    {
        changeTournamentState($id_tournament, 3);
        $tournament['state'] = 3;
    }
}
$smarty->assign('tournament', $tournament);
$smarty->assign('isModerator', isModerator($id_tournament, $id_user['id_user']));
$smarty->assign('isCreator', isCreator($id_tournament, $id_user['id_user']));
$smarty->assign('logged', isset($_SESSION['logged']) ? $_SESSION['logged'] : 0);
$smarty->assign('id_user', isset($id_user) ? $id_user['id_user'] : 0);
$smarty->display('templates/navbar.tpl');
$smarty->display('templates/tournament.tpl');

if ($teams_array && $teams_count > 1)
{//generates and prints bracket
    if (isset($bracket->winner))
    {
        echo('<span id="tournament-winner" class="label label-success tournament-winner">Tournament winner: '.$bracket->winner.'</span>');
    }
    $bracket->updateBracketResults();
    echo $bracket->getBracket();
}



function isCreator($id_tournament, $id_user)
{
    $sql = '
        SELECT *
        FROM tournament
        WHERE id_tournament = "'.(int)$id_tournament.'"
            AND id_creator = "'.(int)$id_user.'"';
    return getValue($sql) ? true : false;
}

function isModerator($id_tournament, $id_user)
{
    $sql = '
        SELECT *
        FROM participant
        WHERE id_tournament = "'.(int)$id_tournament.'"
            AND id_team = "'.(int)$id_user.'"
            AND id_function IN (1,3)';
    return getValue($sql) ? true : false;
}

function getTournamentParticipants($id_tournament, $id_function = 2)
{
    $sql = '
        SELECT u.id_user, u.name, p.id_team
        FROM participant p
        LEFT JOIN user u
        ON u.id_user = p.id_team
        WHERE p.id_tournament = "'.(int)$id_tournament.'"
            AND p.id_function IN ('.$id_function.')
            AND p.state = 1';
//    var_dump($sql);
//    die();
    return executeS($sql);
}

function getTournamentCreator($id_tournament)
{
    $sql = '
        SELECT u.id_user, u.name
        FROM user u
        LEFT JOIN tournament t
        ON t.id_creator = u.id_user
        WHERE t.id_tournament = "'.(int)$id_tournament.'"';

    return executeS($sql);
}

function getTournamentData($id_tournament)
{
    $sql = '
        SELECT t.*, s.name sport
        FROM tournament t
        LEFT JOIN sport s
        ON s.id_sport = t.id_sport
        WHERE t.id_tournament = '.(int)$id_tournament;
    return executeS($sql);
}

function isRegistered($id_user, $id_tournament)
{
    $sql = '
        SELECT *
        FROM tournament trn
        LEFT JOIN participant p
        ON p.id_tournament = trn.id_tournament
        LEFT JOIN user u
        ON u.id_user = p.id_team
        WHERE u.id_user = "'.escape($id_user).'"
            AND trn.id_tournament = "'.escape($id_tournament).'"';
    if (executeS($sql))
        return true;
    return false;
}

function kickParticipant($id_tournament, $id_team)
{
    $sql = '
        UPDATE participant
        SET state = 2
        WHERE id_tournament = "'.(int)$id_tournament.'"
            AND id_team = "'.(int)$id_team.'"';
    return execute($sql);
}

function changeTournamentState($id_tournament, $state)
{
    $sql = '
        UPDATE tournament
        SET state = "'.(int)$state.'"
        WHERE id_tournament = "'.(int)$id_tournament.'"';
    return execute($sql);
}