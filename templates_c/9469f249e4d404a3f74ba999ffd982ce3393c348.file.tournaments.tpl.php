<?php /* Smarty version Smarty-3.1.19, created on 2016-04-12 13:19:38
         compiled from "templates\tournaments.tpl" */ ?>
<?php /*%%SmartyHeaderCode:31971570cd9ca303db6-92206052%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9469f249e4d404a3f74ba999ffd982ce3393c348' => 
    array (
      0 => 'templates\\tournaments.tpl',
      1 => 1401100734,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '31971570cd9ca303db6-92206052',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'title' => 0,
    'tournaments' => 0,
    'tournament' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_570cd9ca40f456_81700390',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_570cd9ca40f456_81700390')) {function content_570cd9ca40f456_81700390($_smarty_tpl) {?><body class="page-body">
<?php echo $_smarty_tpl->getSubTemplate ('navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="row col-lg-12" style="min-height: 60px;"></div>
<div class="col-lg-2"></div>
<div class="container col-lg-8">
	<h2 class="form-new-tournament-heading"><?php if (isset($_smarty_tpl->tpl_vars['title']->value)) {?><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
<?php }?></h2>
	<div class="placeholder20"></div>
	<div class="list-group">
		<div class="well">
			<h4 class="list-group-item-heading">Tournament name</h4>
			<?php if (isset($_smarty_tpl->tpl_vars['tournaments']->value)&&count($_smarty_tpl->tpl_vars['tournaments']->value)>0) {?>
				<?php  $_smarty_tpl->tpl_vars['tournament'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tournament']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tournaments']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tournament']->key => $_smarty_tpl->tpl_vars['tournament']->value) {
$_smarty_tpl->tpl_vars['tournament']->_loop = true;
?>
					<?php if ($_smarty_tpl->tpl_vars['tournament']->value['id_tournament']) {?>
						
						
						
						
						
						
						<a href="tournament.php?id_tournament=<?php echo $_smarty_tpl->tpl_vars['tournament']->value['id_tournament'];?>
" class="list-group-item
						<?php if ($_smarty_tpl->tpl_vars['tournament']->value['state']==0) {?>
							state0
						<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==1) {?>
							state1
						<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==2) {?>
							state2
						<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==3) {?>
							state3
						<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==4) {?>
							state4
						<?php }?>
						"> <?php echo $_smarty_tpl->tpl_vars['tournament']->value['name'];?>
 <span class="badge <?php if ($_smarty_tpl->tpl_vars['tournament']->value['countTeams']>=$_smarty_tpl->tpl_vars['tournament']->value['max_participants']&&$_smarty_tpl->tpl_vars['tournament']->value['max_participants']>0) {?> alert-danger<?php } else { ?> alert-success<?php }?>" title="Participants"><?php echo $_smarty_tpl->tpl_vars['tournament']->value['countTeams'];?>
<?php if ($_smarty_tpl->tpl_vars['tournament']->value['max_participants']>0) {?>/<?php echo $_smarty_tpl->tpl_vars['tournament']->value['max_participants'];?>
<?php }?></span><i class="pull-left list-state-icon fa
						<?php if ($_smarty_tpl->tpl_vars['tournament']->value['state']==0) {?>
							fa-circle-o" title="Not started"
						<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==1) {?>
							fa-play" title="Started"
						<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==2) {?>
							fa-pause" title="Paused"
						<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==3) {?>
							fa-flag-checkered" title="Finished"
						<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==4) {?>
							fa-stop" title="Closed"
						<?php }?>
						></i></a>
					<?php }?>
				<?php } ?>
			<?php }?>
		</div>
	</div>

</div>
</body><?php }} ?>
