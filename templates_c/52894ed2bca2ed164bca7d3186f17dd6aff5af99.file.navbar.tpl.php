<?php /* Smarty version Smarty-3.1.19, created on 2016-05-03 16:37:31
         compiled from "templates\navbar.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10003570cd90025af37-92593434%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '52894ed2bca2ed164bca7d3186f17dd6aff5af99' => 
    array (
      0 => 'templates\\navbar.tpl',
      1 => 1462286251,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10003570cd90025af37-92593434',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_570cd90029cfe1_72027112',
  'variables' => 
  array (
    'errors' => 0,
    'error' => 0,
    'success' => 0,
    's' => 0,
    'logged' => 0,
    'name' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_570cd90029cfe1_72027112')) {function content_570cd90029cfe1_72027112($_smarty_tpl) {?><div class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="margin-bottom: 0 !important;">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">
				<img src="img/logo.png" height="45"/>
			</a>
		</div>
		<div class="col-sm-3 text-center" style="height:1px">
			<?php if (isset($_smarty_tpl->tpl_vars['errors']->value)&&$_smarty_tpl->tpl_vars['errors']->value) {?>
				<div class="alert alert-danger alert-dismissable">
					<?php  $_smarty_tpl->tpl_vars['error'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['error']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['errors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['error']->key => $_smarty_tpl->tpl_vars['error']->value) {
$_smarty_tpl->tpl_vars['error']->_loop = true;
?>
						<?php echo $_smarty_tpl->tpl_vars['error']->value;?>
<br/>
					<?php } ?>
				</div>
			<?php } elseif (isset($_smarty_tpl->tpl_vars['success']->value)&&$_smarty_tpl->tpl_vars['success']->value) {?>
				<div class="alert alert-success alert-dismissable">
					<?php  $_smarty_tpl->tpl_vars['s'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['s']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['success']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['s']->key => $_smarty_tpl->tpl_vars['s']->value) {
$_smarty_tpl->tpl_vars['s']->_loop = true;
?>
						<?php echo $_smarty_tpl->tpl_vars['s']->value;?>
<br/>
					<?php } ?>
				</div>
			<?php }?>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav pull-right">
				<li>
					<a href="findTournament.php"><i class="fa fa-search"></i> Find a tournament</a>
				</li>
				<?php if (!$_smarty_tpl->tpl_vars['logged']->value) {?>
					<li>
						<div class="navbar-form">
							<form method="post" action="process.php" class="form-inline" role="form" name="login-form" id="login-form">
								<div class="form-group">
									<label class="sr-only" for="login-email">Email address</label>
									<input type="email" class="form-control input-sm" id="login-email" placeholder="Enter email" name="email">
								</div>
								<div class="form-group">
									<label class="sr-only" for="login-pwd">Password</label>
									<input type="password" class="form-control input-sm" id="login-pwd" placeholder="Password" name="password">
								</div>
								<button type="submit" class="btn btn-primary btn-sm" name="submit-login">Sign in</button>
							</form>
						</div> <!-- /container -->
					</li>
					<li><a href="register.php">Register</a></li>
				<?php } else { ?>
					<li>
						<a href="createTournament.php"><i class="fa fa-trophy"></i> Create a tournament</a>
					</li>
					<li>
						<a href="" class="navbar-text-custom dropdown-toggle"  data-toggle="dropdown">
							<?php if (isset($_smarty_tpl->tpl_vars['name']->value)) {?>
								<i class="glyphicon glyphicon-user"></i>
								<?php echo $_smarty_tpl->tpl_vars['name']->value;?>

							<?php }?>
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu navbar-inverse">
							<li><a href="myTournaments.php" class="hover-color-custom navbar-text-custom">My tournaments</a></li>
							<li class="divider"></li>
							<li><a href="#" class="hover-color-custom navbar-text-custom"><i class="glyphicon glyphicon-cog"></i> Settings</a></li>
						</ul>
					</li>
					<li><a href="logout.php"><i class="glyphicon glyphicon-off"></i> Logout</a></li>
				<?php }?>
			</ul>
		</div><!--/.nav-collapse -->
	</div>
</div><?php }} ?>
