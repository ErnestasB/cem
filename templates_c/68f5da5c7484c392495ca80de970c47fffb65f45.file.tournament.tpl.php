<?php /* Smarty version Smarty-3.1.19, created on 2016-04-12 13:28:08
         compiled from "templates\tournament.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17161570cdbc8f1bde7-70599815%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '68f5da5c7484c392495ca80de970c47fffb65f45' => 
    array (
      0 => 'templates\\tournament.tpl',
      1 => 1401100734,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17161570cdbc8f1bde7-70599815',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'tournament' => 0,
    'logged' => 0,
    'isModerator' => 0,
    'isCreator' => 0,
    'team' => 0,
    'id_user' => 0,
    'moderator' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_570cdbc952d971_29847367',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_570cdbc952d971_29847367')) {function content_570cdbc952d971_29847367($_smarty_tpl) {?><script>
	var id_tournament = <?php echo $_smarty_tpl->tpl_vars['tournament']->value['id_tournament'];?>
;
	var canEdit = Number("<?php echo $_smarty_tpl->tpl_vars['logged']->value&&($_smarty_tpl->tpl_vars['isModerator']->value||$_smarty_tpl->tpl_vars['isCreator']->value)&&$_smarty_tpl->tpl_vars['tournament']->value['state']==1;?>
");
</script>

<body class="page-body">
<div class="row col-lg-12 placeholder60"></div>

	<?php if ($_smarty_tpl->tpl_vars['tournament']->value['isCreator']) {?>

		<div class="row">
			<div class="col-lg-3"></div>
			<form method="post" action="process.php" id="tournament-creator-actions">
				<input type="hidden" name="id_tournament" value="<?php echo $_smarty_tpl->tpl_vars['tournament']->value['id_tournament'];?>
"/>
				<div class="btn-group before-button">
					<?php if ($_smarty_tpl->tpl_vars['tournament']->value['state']!=4&&$_smarty_tpl->tpl_vars['tournament']->value['state']!=3) {?>
						<?php if (($_smarty_tpl->tpl_vars['tournament']->value['max_participants']==0||$_smarty_tpl->tpl_vars['tournament']->value['countTeams']<$_smarty_tpl->tpl_vars['tournament']->value['max_participants'])&&$_smarty_tpl->tpl_vars['tournament']->value['state']==0) {?>
							<button type="button" id="registerToTournament" class="btn btn-primary"><i class="fa fa-plus"></i> Register a participant</button>
						<?php }?>
						<button type="button" id="registerModerator" class="btn btn-default"><i class="fa fa-plus-circle"></i> Register tournament moderators</button>
						<?php if ($_smarty_tpl->tpl_vars['tournament']->value['state']==0) {?>
							<button type="submit" name="submitStartTournament" class="btn btn-success"><i class="fa fa-play"></i> Start tournament</button>
						<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==1) {?>
							<button type="submit" name="submitPauseTournament" class="btn btn-warning"><i class="fa fa-pause"></i> Pause tournament</button>
						<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==2) {?>
							<button type="submit" name="submitResumeTournament" class="btn btn-success"><i class="fa fa-play"></i> Resume tournament</button>
						<?php }?>
						<button type="submit" id="submitCloseTournament" name="submitCloseTournament" class="btn btn-danger "><i class="fa fa-minus-circle"></i> Close tournament</button>
					<?php }?>
				</div>
			</form>
		</div>
		<div class="placeholder20"></div>
	<?php } else { ?>

		<div class="row">
			<div class="col-lg-4"></div>
			<?php if (isset($_smarty_tpl->tpl_vars['logged']->value)&&$_smarty_tpl->tpl_vars['logged']->value) {?>
				<div class="btn-group before-button">
					<form method="post" action="process.php" id="tournament-registration">
						<input type="hidden" name="id_tournament" value="<?php echo $_smarty_tpl->tpl_vars['tournament']->value['id_tournament'];?>
"/>
						<?php if ($_smarty_tpl->tpl_vars['tournament']->value['open_registration']==1&&!$_smarty_tpl->tpl_vars['tournament']->value['isRegistered']&&$_smarty_tpl->tpl_vars['tournament']->value['state']==0&&($_smarty_tpl->tpl_vars['tournament']->value['max_participants']==0||$_smarty_tpl->tpl_vars['tournament']->value['countTeams']<$_smarty_tpl->tpl_vars['tournament']->value['max_participants'])) {?>
							<button type="submit" id="submitTournamentRegistration" name="submitTournamentRegistration" class="btn btn-primary"><i class="fa fa-plus"></i> Register to this tournament</button>
						<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['isRegistered']&&!$_smarty_tpl->tpl_vars['isModerator']->value&&$_smarty_tpl->tpl_vars['tournament']->value['state']==0) {?>
							<button type="submit" class="btn btn-danger " id="submitTournamentDeregistration" name="submitTournamentDeregistration"><i class="fa fa-minus-circle"></i> Stop participating in this tournament</button>
						<?php }?>
					</form>
				</div>
			<?php }?>
		</div>

	<?php }?>

	<div class="col-lg-4 well">
		
		
		
		
		
		
		<h3 class="participants-heading">&nbsp<i class="fa fa-users"></i> Information <?php if ($_smarty_tpl->tpl_vars['tournament']->value['isCreator']&&!in_array($_smarty_tpl->tpl_vars['tournament']->value['state'],array(3,4))) {?><i id="edit-info" title="Edit information" class="fa fa-edit pull-right"></i><?php }?></h3>
		<h5 class="info-line">Tournament name: <span class="pull-right"><?php echo $_smarty_tpl->tpl_vars['tournament']->value['name'];?>
</span></h5>
		<h5 class="info-line">Sport/Game: <span class="pull-right"><?php echo $_smarty_tpl->tpl_vars['tournament']->value['sport'];?>
</span></h5>
		<?php if ($_smarty_tpl->tpl_vars['tournament']->value['description']) {?><h5 ">Description:</h5> <div  class="tournament-description clearfix info-line"><?php echo $_smarty_tpl->tpl_vars['tournament']->value['description'];?>
</div><?php }?>
		<?php if ($_smarty_tpl->tpl_vars['tournament']->value['start_date']&&$_smarty_tpl->tpl_vars['tournament']->value['start_date']!='0000-00-00 00:00:00') {?><h5 class="info-line">Start date: <span class="pull-right"><?php echo $_smarty_tpl->tpl_vars['tournament']->value['start_date'];?>
</span></h5><?php }?>
		<h5 class="info-line">Tournament state:
			<?php if ($_smarty_tpl->tpl_vars['tournament']->value['state']==0) {?>
				<i class="fa fa-circle-o pull-right"><span class="state-text"> Not started</span></i>
			<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==1) {?>
				<i class="fa fa-play pull-right"><span class="state-text"> Started</span></i>
			<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==2) {?>
				<i class="fa fa-pause pull-right"><span class="state-text"> Paused</span></i>
			<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==3) {?>
				<i class="fa fa-flag-checkered pull-right"><span class="state-text"> Finished</span></i>
			<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==4) {?>
				<i class="fa fa-stop pull-right"><span class="state-text"> Closed</span></i>
			<?php }?>
		</h5>
		<h3 class="participants-heading">&nbsp<i class="fa fa-users"></i> Tournament participants</h3>
		<div class="progress">
			<div class="progress-bar <?php if ($_smarty_tpl->tpl_vars['tournament']->value['max_participants']&&$_smarty_tpl->tpl_vars['tournament']->value['countTeams']>=$_smarty_tpl->tpl_vars['tournament']->value['max_participants']) {?>progress-bar-danger<?php } else { ?>progress-bar-info<?php }?>" role="progressbar"
				 aria-valuenow="<?php echo $_smarty_tpl->tpl_vars['tournament']->value['countTeams'];?>
"
				 aria-valuemin="0"
				 aria-valuemax="<?php echo $_smarty_tpl->tpl_vars['tournament']->value['max_participants'];?>
"
				 style="width: <?php if ($_smarty_tpl->tpl_vars['tournament']->value['max_participants']>0) {?><?php echo ($_smarty_tpl->tpl_vars['tournament']->value['countTeams']*100)/$_smarty_tpl->tpl_vars['tournament']->value['max_participants'];?>
<?php } else { ?>100<?php }?>%">

				<span class="progress-bar-text"><?php if ($_smarty_tpl->tpl_vars['tournament']->value['max_participants']>0) {?><?php echo $_smarty_tpl->tpl_vars['tournament']->value['countTeams'];?>
 / <?php echo $_smarty_tpl->tpl_vars['tournament']->value['max_participants'];?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['tournament']->value['countTeams'];?>
<?php }?></span>

			</div>
		</div>
		<?php if ($_smarty_tpl->tpl_vars['tournament']->value['countTeams']==0) {?>
			<div class="col-lg-1"></div>
			<div class="text-center col-lg-9 alert alert-warning no-teams">This tournament has no participants yet</div>
		<?php } else { ?>
			<ul class="list-group">
				<form method="post" action="process.php">
					<input type="hidden" name="id_tournament" value="<?php echo $_smarty_tpl->tpl_vars['tournament']->value['id_tournament'];?>
"/>
					<?php  $_smarty_tpl->tpl_vars['team'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['team']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tournament']->value['teams']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['team']->key => $_smarty_tpl->tpl_vars['team']->value) {
$_smarty_tpl->tpl_vars['team']->_loop = true;
?>
						<li class="list-group-item" id="<?php echo $_smarty_tpl->tpl_vars['team']->value['name'];?>
">
							<?php echo $_smarty_tpl->tpl_vars['team']->value['name'];?>

							<?php if ($_smarty_tpl->tpl_vars['tournament']->value['state']==0&&$_smarty_tpl->tpl_vars['id_user']->value==$_smarty_tpl->tpl_vars['tournament']->value['id_creator']) {?>
								<button type="submit" name="removeUser" value="<?php echo $_smarty_tpl->tpl_vars['team']->value['id_team'];?>
" class="btn btn-danger btn-sm pull-right" title="Remove this user">
									<i class="fa fa-minus" ></i>
								</button>
							<?php }?>
						</li>
					<?php } ?>
				</form>
			</ul>
		<?php }?>

		<h3 class="participants-heading">&nbsp<i class="fa fa-user-md"></i> Tournament moderators</h3>
		<div class="progress">
			<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width: 100%">
				<span class="progress-bar-text"><?php echo $_smarty_tpl->tpl_vars['tournament']->value['countMods'];?>
</span>
			</div>
		</div>
		<?php if ($_smarty_tpl->tpl_vars['tournament']->value['countMods']==0) {?>
			<div class="col-lg-1"></div>
			<div class="text-center col-lg-9 alert alert-warning no-teams">This tournament has no moderators yet</div>
		<?php } else { ?>
			<ul class="list-group">
				<form method="post" action="process.php">
					<input type="hidden" name="id_tournament" value="<?php echo $_smarty_tpl->tpl_vars['tournament']->value['id_tournament'];?>
"/>
					<?php  $_smarty_tpl->tpl_vars['moderator'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['moderator']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tournament']->value['mods']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['moderator']->key => $_smarty_tpl->tpl_vars['moderator']->value) {
$_smarty_tpl->tpl_vars['moderator']->_loop = true;
?>
						<li class="list-group-item" id="<?php echo $_smarty_tpl->tpl_vars['moderator']->value['name'];?>
">
							<?php echo $_smarty_tpl->tpl_vars['moderator']->value['name'];?>

							<?php if ($_smarty_tpl->tpl_vars['moderator']->value['id_user']!=$_smarty_tpl->tpl_vars['tournament']->value['id_creator']&&$_smarty_tpl->tpl_vars['id_user']->value==$_smarty_tpl->tpl_vars['tournament']->value['id_creator']) {?>
								<button type="submit" name="removeMod" value="<?php echo $_smarty_tpl->tpl_vars['moderator']->value['id_user'];?>
" class="btn btn-danger btn-sm pull-right" title="Remove this moderator">
									<i class="fa fa-minus" ></i>
								</button>
							<?php }?>
						</li>
					<?php } ?>
				</form>
			</ul>
		<?php }?>
	</div>

</body><?php }} ?>
