<?php /* Smarty version Smarty-3.1.19, created on 2016-04-12 13:16:16
         compiled from "templates\main.tpl" */ ?>
<?php /*%%SmartyHeaderCode:514570cd9000c8d92-21877018%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd3ecc655cf081d350438ae4197ceb36a188f9d96' => 
    array (
      0 => 'templates\\main.tpl',
      1 => 1401100734,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '514570cd9000c8d92-21877018',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'todaysTournaments' => 0,
    'tournament' => 0,
    'popularSports' => 0,
    'sport' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_570cd900186ed9_40007749',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_570cd900186ed9_40007749')) {function content_570cd900186ed9_40007749($_smarty_tpl) {?><body class="page-body">
<?php echo $_smarty_tpl->getSubTemplate ('navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="row col-lg-12" style="min-height: 60px;"></div>
<div class="placeholder80"></div>

<div class="col-lg-6">
	<div class="list-group">
		<div class="well well-lg">
			<h2 class="text-center">Today's tournaments</h2>
			<br/>
			<table class="todays-table">
				<tr class="todays-row">
					<td class="list-group-item-heading todays-heading">Tournament name</td>
					<td class="list-group-item-heading todays-heading">Sport or game</td>
					<td class="list-group-item-heading pull-right todays-heading todays-heading-last">Start date</td>
				</tr>
			</table>
			<?php  $_smarty_tpl->tpl_vars['tournament'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tournament']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['todaysTournaments']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tournament']->key => $_smarty_tpl->tpl_vars['tournament']->value) {
$_smarty_tpl->tpl_vars['tournament']->_loop = true;
?>
				<?php if ($_smarty_tpl->tpl_vars['tournament']->value['id_tournament']) {?>
					
					
					
					
					
					
					<a href="tournament.php?id_tournament=<?php echo $_smarty_tpl->tpl_vars['tournament']->value['id_tournament'];?>
">
						<table class="todays-table">
							<tr class="todays-list-row list-group-item
								<?php if ($_smarty_tpl->tpl_vars['tournament']->value['state']==0) {?>
									state0
								<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==1) {?>
									state1
								<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==2) {?>
									state2
								<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==3) {?>
									state3
								<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==4) {?>
									state4
								<?php }?>
							">
								<td class="todays-row-td-first">
									<i class="fa
									<?php if ($_smarty_tpl->tpl_vars['tournament']->value['state']==0) {?>
										fa-circle-o" title="Not started"
									<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==1) {?>
										fa-play" title="Started"
									<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==2) {?>
										fa-pause" title="Paused"
									<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==3) {?>
										fa-flag-checkered" title="Finished"
									<?php } elseif ($_smarty_tpl->tpl_vars['tournament']->value['state']==4) {?>
										fa-stop" title="Closed"
									<?php }?>
									> </i> <?php echo $_smarty_tpl->tpl_vars['tournament']->value['name'];?>
</td>
								<td class="todays-row-td-middle"><?php echo $_smarty_tpl->tpl_vars['tournament']->value['sport'];?>
</td>
								<td class="todays-row-td-last"><?php if (($_smarty_tpl->tpl_vars['tournament']->value['start_date'])!='0000-00-00 00:00:00') {?><?php echo $_smarty_tpl->tpl_vars['tournament']->value['start_date'];?>
<?php } else { ?> -- <?php }?></td>
							</tr>
						</table>
					</a>
				<?php }?>
			<?php } ?>
		</div>
	</div>
</div>


<div class="col-lg-6">
	<div class="list-group">
		<div class="well well-lg">
			<h2 class="text-center">Overall statistics</h2>
			<br/>
			<h4 class="media-heading ">Sport or game <span class="media-heading pull-right">Number of tournaments</span></h4>
			<?php  $_smarty_tpl->tpl_vars['sport'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sport']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['popularSports']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sport']->key => $_smarty_tpl->tpl_vars['sport']->value) {
$_smarty_tpl->tpl_vars['sport']->_loop = true;
?>
				<div class="list-group-item">
					<?php echo $_smarty_tpl->tpl_vars['sport']->value['name'];?>

					<div class="badge alert-info pull-right"><?php echo $_smarty_tpl->tpl_vars['sport']->value['game_count'];?>
</div>
				</div>

			<?php } ?>
		</div>
	</div>
</div>

</body><?php }} ?>
