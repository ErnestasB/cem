<?php /* Smarty version Smarty-3.1.19, created on 2016-04-12 13:20:35
         compiled from "templates\new-tournament.tpl" */ ?>
<?php /*%%SmartyHeaderCode:23961570cda035d9b28-13468987%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6e90287584abf05715fed0476505787224029dc4' => 
    array (
      0 => 'templates\\new-tournament.tpl',
      1 => 1401100734,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '23961570cda035d9b28-13468987',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'sports' => 0,
    'sport' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_570cda03648661_64436635',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_570cda03648661_64436635')) {function content_570cda03648661_64436635($_smarty_tpl) {?><body class="page-body">
<?php echo $_smarty_tpl->getSubTemplate ('navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="row col-lg-12" style="min-height: 60px;"></div>
<div class="col-lg-2"></div>
<div class="container col-lg-8">
	<form method="post" action="process.php" class="form-inline" role="form" name="new-tournament-form" id="new-tournament-form">
		<h2 class="form-new-tournament-heading">Create a tournament</h2>
		<div class="placeholder50"></div>
		<div class="control-group">

			<label class="control-label label-left" for="tournament-name">Tournament name</label>
			<div class="control">
				<div class="input-group inline-flex">
					<input type="text" class="form-control tournament-input-sm" id="tournament-name" placeholder="Tournament name" name="tournament-name" required>
					<span class="input-group-addon form-control">
						<i class="fa fa-font"></i>
					</span>
				</div>
			</div>
			<div class="placeholder20"></div>
			<div class="clearfix"></div>

			<label class="control-label label-left" for="tournament-sport">Game</label>
			<div class="control">
				<div class="input-group inline-flex">
					<select class="form-control tournament-input-sm" id="tournament-sport" placeholder="The game or sport of tournament" name="tournament-sport">
						<?php  $_smarty_tpl->tpl_vars['sport'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sport']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sports']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sport']->key => $_smarty_tpl->tpl_vars['sport']->value) {
$_smarty_tpl->tpl_vars['sport']->_loop = true;
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['sport']->value['id_sport'];?>
"><?php echo $_smarty_tpl->tpl_vars['sport']->value['name'];?>
</option>
						<?php } ?>
					</select>
					<span class="input-group-addon form-control">
						<i class="fa fa-gamepad"></i>
					</span>
				</div>
			</div>
			<div class="placeholder20"></div>
			<div class="clearfix"></div>

			<label class="control-label label-left" for="tournament-participants">Max. number of participants</label>
			<div class="control">
				<div class="input-group inline-flex">
					<input type="number" class="form-control tournament-input-xs" id="tournament-participants" placeholder="Leave blank for unlimited" name="tournament-participants">
					<span class="input-group-addon form-control">
						<i class="fa fa-users"></i>
					</span>
				</div>
			</div>
			<div class="placeholder20"></div>
			<div class="clearfix"></div>

			<label class="control-label label-left" for="tournament-desc">Description</label>
			<div class="control mce-parent">
				<textarea class="form-control" id="tournament-desc" placeholder="Describe this tournament" name="tournament-desc"></textarea>
			</div>
			<div class="placeholder20"></div>
			<div class="clearfix"></div>

			<label class="control-label label-left" for="datetimepicker">Tournament start date</label>
			<div class="control">
				<div class="input-group inline-flex">
				<input id="datetimepicker" class="form-control" placeholder="Click to pick a date" type="text" name="tournament-start-date">
				<span class="input-group-addon form-control">
					<i class="glyphicon glyphicon-calendar"></i>
				</span>
				</div>
			</div>
			<div class="clearfix"></div>

		</div>
		<div class="placeholder20"></div>

		<label class="label-left"></label>
		<button type="submit" class="btn btn-primary tournament-input-md" name="submit-new-tournament">Create</button>
	</form>
</div>
</body><?php }} ?>
