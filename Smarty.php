<?php
if (!isset($_SESSION['access']))
    header('Location: index.php');


require_once('smarty/Smarty.class.php');
require_once('connect.php');
require_once('Bracket.php');

global $smarty;

$smarty = new Smarty;
$smarty->template_dir = 'templates';
$smarty->config_dir = 'config';
$smarty->cache_dir = 'cache';
$smarty->compile_dir = 'templates_c';
$smarty->caching = 0;
$smarty->display('templates/includes.tpl');
$smarty->assign(array(
    'logged' => isset($_SESSION['logged']) ? $_SESSION['logged'] : false,
    'name' => isset($_SESSION['userName']) ? $_SESSION['userName'] : '',
));
?>

<!-- TinyMCE -->
<script src="js/tinymce/tinymce.min.js"></script>
<script>
    tinymce.init(
        {selector:'textarea', menubar:'false', width:'550'}
    );
</script>

<!-- Date/time picker -->
<link rel="stylesheet" type="text/css" href="datetimepicker-master/jquery.datetimepicker.css"/ >
<script src="datetimepicker-master/jquery.datetimepicker.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.bpopup.min.js"></script>
<script src="jquery-custom-scrollbar-0.5.5/jquery.custom-scrollbar.min.js"></script>
<link type="text/css" rel="stylesheet" href="jquery-custom-scrollbar-0.5.5/jquery.custom-scrollbar.css"/>
<script src="js/main.js"></script>