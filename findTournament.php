<?php

if (session_status() == PHP_SESSION_NONE)
{
    session_start();
}

unset($_SESSION['id_tournament']);
unset($_SESSION['tournamentRegSuccess']);
require_once('configs/configs.php');
require_once('Smarty.php');
$tournaments = getAllTournaments();
$smarty->assign(array(
    'tournaments' => $tournaments ? $tournaments : array(),
    'title' => 'Tournaments list'
));
$smarty->display('templates/tournaments.tpl');

function getAllTournaments()
{
    $sql = '
        SELECT t.*, COUNT(p.id_team) countTeams
        FROM tournament t
        LEFT JOIN participant p
        ON p.id_tournament = t.id_tournament
          AND p.id_function = 2
        WHERE t.state != 4
        GROUP BY t.id_tournament';
    return executeS($sql);
}