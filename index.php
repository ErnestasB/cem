<?php

require_once('configs/configs.php');
require_once('Smarty.php');

$smarty->assign(array(
    'email' => isset($_SESSION['userEmail']) ? $_SESSION['userEmail'] : '',
    'errors' => isset($_SESSION['errors']) ? array_unique($_SESSION['errors']) : array(),
    'success' => isset($_SESSION['success']) ? array_unique($_SESSION['success']) : array(),
    'name' => isset($_SESSION['userName']) ? $_SESSION['userName'] : '',
    'logged' => isset($_SESSION['logged']) ? $_SESSION['logged'] : false
));
if (isset($_SESSION['errors']))
    unset($_SESSION['errors']);
if (isset($_SESSION['success']))
    unset($_SESSION['success']);
$smarty->assign(array(
    'popularSports' => getPopularSports(),
    'todaysTournaments' => getTodaysTournaments()
));
$smarty->display('templates/main.tpl');


function getPopularSports()
{
    $sql = '
        SELECT DISTINCT s.name, COUNT(t.id_sport) game_count
        FROM tournament t
        INNER JOIN sport s
        ON t.id_sport = s.id_sport
        GROUP BY s.name
        ORDER BY game_count DESC';

    return executeS($sql);
}

function getTodaysTournaments()
{

    $today = date('Y-m-d 00:00:00');
    $sql = '
        SELECT t.id_tournament, t.state, t.name name, t.creation_date, t.start_date, s.name sport
        FROM tournament t
        LEFT JOIN sport s
        ON s.id_sport = t.id_sport
        WHERE t.creation_date >= "'.$today.'"
        ORDER BY start_date ASC';

    return executeS($sql);
}