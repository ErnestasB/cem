<?php
if (session_status() == PHP_SESSION_NONE)
{
    session_start();
}
if (!isset($_SESSION['access']))
    header('Location: index.php');

$host = 'localhost';
$dbname = 'uni_bbd_cem';
$user = 'root';
$password = 'root';
$GLOBALS['db'] = mysqli_connect($host, $user, $password, $dbname);
if (!$GLOBALS['db'])
    exit ('Unable to connect to database');

function execute($sql)
{
    return (bool)mysqli_query($GLOBALS['db'], $sql);
}

function executeS($sql)
{
    $result = mysqli_query($GLOBALS['db'], $sql);
    $return = array();
    if ($result) //Check if the return is true
        while($row = mysqli_fetch_assoc($result))
            $return[] = $row;
    return $return;
}

function getValue($sql)
{
    $result = mysqli_query($GLOBALS['db'], $sql);
    if ($result)
        return mysqli_fetch_assoc($result);
    return false;
}

function escape($var)
{
    return mysqli_real_escape_string($GLOBALS['db'], $var);
}

function getLastId()
{
    return mysqli_insert_id($GLOBALS['db']);
}

function getSalt()
{
    $sql = 'SELECT `salt` FROM `salt`';
    $salt_array = getValue($sql);
    return $salt_array['salt'];
}